angular.module('app.routes', [])

.config(function ($stateProvider, $urlRouterProvider) {

    // Ionic uses AngularUI Router which uses the concept of states
    // Learn more here: https://github.com/angular-ui/ui-router
    // Set up the various states which the app can be in.
    // Each state's controller can be found in controllers.js
    $stateProvider


        .state('menu.registration', {
            url: '/page1',
            views: {
                'side-menu21': {
                    templateUrl: 'templates/registration.html',
                    controller: 'registrationCtrl'
                }
            }
        })
    .state('menu.feedback', {
            url: '/feedback',
            views: {
                'side-menu21': {
                    templateUrl: 'templates/feedback.html',
                    controller: 'feedbackCtrl'
                }
            }
        })
    .state('menu.cart', {
        url: '/page2',
        views: {
            'side-menu21': {
                templateUrl: 'templates/cart.html',
                controller: 'cartCtrl'
            }
        }
    })

    .state('menu.changemobilenumber', {
        url: '/changemobilenumber',
        views: {
            'side-menu21': {
                templateUrl: 'templates/changemobilenumber.html',
                controller: 'changemobilenumberCtrl'
            }
        }
    })
   .state('menu.manageroles', {
       url: '/manageroles',
       views: {
           'side-menu21': {
               templateUrl: 'templates/manageroles.html',
               controller: 'managerolesCtrl'
           }
       }
   })
    .state('menu.reports', {
            url: '/reports',
            views: {
                'side-menu21': {
                    templateUrl: 'templates/reports.html',
                    controller: 'reportsCtrl'
                }
            }
        })
    .state('menu.home', {
        url: '/home',
        views: {
            'side-menu21': {
                templateUrl: 'templates/home.html',
                controller: 'homeCtrl'
            }
        }
    })
  .state('menu.myaccount', {
      url: '/myaccount',
      views: {
          'side-menu21': {
              templateUrl: 'templates/myaccount.html',
              controller: 'myaccountCtrl'
          }
      }
  })
    .state('menu', {
        url: '/side-menu21',
        templateUrl: 'templates/menu.html',
        controller: 'menuCtrl'
    })
     .state('menu.login', {
         url: '/login',
         views: {
             'side-menu21': {
                 templateUrl: 'templates/login.html',
                 controller: 'loginCtrl'
             }
         }

     })
      .state('menu.changepassword', {
          url: '/changepassword',
          views: {
              'side-menu21': {
                  templateUrl: 'templates/changepassword.html',
                  controller: 'changepasswordCtrl'
              }
          }

      })
        .state('menu.demo', {
            url: '/demo',
            views: {
                'side-menu21': {
                    templateUrl: 'templates/demo.html',
                    controller: 'demoCtrl'
                }
            }

        })
        .state('menu.events', {
            url: '/events',
            views: {
                'side-menu21': {
                    templateUrl: 'templates/events.html',
                    controller: 'eventsCtrl'
                }
            }
        })
        .state('menu.superadmin', {
            url: '/superadmin',
            views: {
                'side-menu21': {
                    templateUrl: 'templates/superadmin.html',
                    controller: 'superadminCtrl'
                }
            }
        })
       .state('menu.forgotpassword', {
           url: '/forgotpassword',

           views: {
               'side-menu21': {
                   templateUrl: 'templates/forgotpassword.html',
                   controller: 'forgotpasswordCtrl'
               }
           }

       })
       .state('menu.eventattendance', {
        url: '/eventattendance',

        views: {
            'side-menu21': {
                templateUrl: 'templates/eventattendance.html',
                controller: 'eventattendanceCtrl'
            }
        }

    })
    .state('menu.eventslist', {
        url: '/eventlist',

        views: {
            'side-menu21': {
                templateUrl: 'templates/eventlist.html',
                controller: 'eventslistCtrl'
            }
        }

    })
    .state('menu.eventfeedback', {
        url: '/eventfeedback',

        views: {
            'side-menu21': {
                templateUrl: 'templates/eventfeedback.html',
                controller: 'eventfeedbackCtrl'
            }
        }

    })
    .state('menu.eventupload', {
        url: '/eventupload',

        views: {
            'side-menu21': {
                templateUrl: 'templates/eventupload.html',
                controller: 'eventuploadCtrl'
            }
        }

    })
      .state('menu.homeforlogin', {
          url: '/homeforlogin',
          params: {
            obj: null
          },
          views: {
              'side-menu21': {
                  templateUrl: 'templates/homeforlogin.html',
                  controller: 'homeforloginCtrl'
              }
          }
      })
      .state('menu.voterlist', {
          url: '/voterlist',
          views: {
              'side-menu21': {
                  templateUrl: 'templates/voterlist.html',
                  controller: 'VoterlistCtrl'
              }
          }
      })
      
        .state('menu.vistarak', {
            url: '/vistarak',
            views: {
                'side-menu21': {
                    templateUrl: 'templates/vistarakReport.html',
                    controller: 'vistarakCtrl'
                }
            }
        })
      .state('menu.activemebership', {
        url: '/activemembership',
        views: {
            'side-menu21': {
                templateUrl: 'templates/activemembershipform.html',
                controller: 'activemembershipCtrl'
            }
        }
    })
    $urlRouterProvider.otherwise('/side-menu21/home')


});