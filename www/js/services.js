angular.module('app.services', [])


.factory('BlankFactory', ['$http', 'Upload', '$q', function ($http, Upload, $q, $scope) {
    
   //var url="http://10.10.10.152:8001/"
    var url="http://202.153.34.166:8002/"
   //var url="http://apps.apbjpdata.org/"
   var apiUrl = url+"services/";
  //var apiUrl = "http://localhost:8000/services/";

  //var apiUrl = "http://10.10.11.210:8000/services/";
    //var apiUrl = "http://10.10.11.222:8001/services/"; //membership / changepasswordhttp://188.42.96.92/
   //var apiUrl = "http://202.153.34.166:8001/services/";
   //var apiUrl = "http://188.42.96.92/services/";

   var apiurl2 = "http://188.42.96.92/";
 // var apiurl2 = "http://202.153.34.166:8001/";
    return {
        //BW---booth worker
        getIdProofDetails: function () {
            // the $http API is based on the deferred/promise APIs exposed by the $q service
            // so it returns a promise for us by default
            return $http.get(apiUrl + "idproof/choices")
                .then(function (response) {
                    if (typeof response.data === 'object') {

                        return response.data;

                    } else {
                        // invalid response
                        return $q.reject(response.data);
                    }

                }, function (response) {
                    // something went wrong
                    return $q.reject(response.data);
                });
        },
        getUrl:function(){
           
            return url;
        },
        setUserRole:function(role){
            
            window.localStorage.setItem('role',role)
         },
         getUserRole:function(role){
            
           return  window.localStorage.getItem('role')
         },
         
         getUserProfessionList: function () {
            // the $http API is based on the deferred/promise APIs exposed by the $q service
            // so it returns a promise for us by default
            return $http.get(apiUrl + "profession/choices")
                .then(function (response) {
                    if (typeof response.data === 'object') {

                        return response.data;

                    } else {
                        // invalid response
                        return $q.reject(response.data);
                    }

                }, function (response) {
                    // something went wrong
                    return $q.reject(response.data);
                });
        },
         getfeedbackcategoryList: function () {
            // the $http API is based on the deferred/promise APIs exposed by the $q service
            // so it returns a promise for us by default
            return $http.get(apiUrl + "feedbacktype/")
                .then(function (response) {
                    if (typeof response.data === 'object') {

                        return response.data;

                    } else {
                        // invalid response
                        return $q.reject(response.data);
                    }

                }, function (response) {
                    // something went wrong
                    return $q.reject(response.data);
                });
        },
     
        getwebsite: function (id,password) {
            // the $http API is based on the deferred/promise APIs exposed by the $q service
            // so it returns a promise for us by default
            return $http.get(apiurl2+"accounts/login/lau/" + id + "/" + password)
                .then(function (response) {
                    if (response.data == 'urlmsg') {

                        return response.data;

                    } else {
                        // invalid response
                        return $q.reject(response.data);
                    }

                }, function (response) {
                    // something went wrong
                    return $q.reject(response.data);
                });
        },
        getNews: function () {
            // the $http API is based on the deferred/promise APIs exposed by the $q service
            // so it returns a promise for us by default
            return $http.get("https://api.rss2json.com/v1/api.json?rss_url=http://www.bjp.org/?option=com_ninjarsssyndicator&feed_id=3&format=raw")
                .then(function (response) {
                    if (typeof response.data === 'object') {

                        return response.data;

                    } else {
                        // invalid response
                        return $q.reject(response.data);
                    }

                }, function (response) {
                    // something went wrong
                    return $q.reject(response.data);
                });
        },

        getflashNews: function () {
            // the $http API is based on the deferred/promise APIs exposed by the $q service
            // so it returns a promise for us by default
            return $http.get(apiUrl + "event_list")
                .then(function (response) {
                    if (typeof response.data === 'object') {

                        return response.data;

                    } else {
                        // invalid response
                        return $q.reject(response.data);
                    }

                }, function (response) {
                    // something went wrong
                    return $q.reject(response.data);
                });
        },
        getAllRoles: function () {
            // the $http API is based on the deferred/promise APIs exposed by the $q service
            // so it returns a promise for us by default
            return $http.get(apiUrl + "admin/roles/all")
                .then(function (response) {
                    if (typeof response.data === 'object') {

                        return response.data;

                    } else {
                        // invalid response
                        return $q.reject(response.data);
                    }

                }, function (response) {
                    // something went wrong
                    return $q.reject(response.data);
                });
        },
        getAllCast: function () {
            // the $http API is based on the deferred/promise APIs exposed by the $q service
            // so it returns a promise for us by default
            return $http.get(apiUrl + "cast/choices")
                .then(function (response) {
                    if (typeof response.data === 'object') {

                        return response.data;

                    } else {
                        // invalid response
                        return $q.reject(response.data);
                    }

                }, function (response) {
                    // something went wrong
                    return $q.reject(response.data);
                });
        },
        deleteRoleById: function (id) {
            // the $http API is based on the deferred/promise APIs exposed by the $q service
            // so it returns a promise for us by default
            return $http.get(apiUrl + "admin/roles/delete/" + id)
                .then(function (response) {
                    if (typeof response.data === 'object') {

                        return response.data;

                    } else {
                        // invalid response
                        return $q.reject(response.data);
                    }

                }, function (response) {
                    // something went wrong
                    return $q.reject(response.data);
                });
        },
        getRoleById: function (id) {
            // the $http API is based on the deferred/promise APIs exposed by the $q service
            // so it returns a promise for us by default
            return $http.get(apiUrl + "admin/roles/view/"+id)
                .then(function (response) {
                    if (typeof response.data === 'object') {

                        return response.data;

                    } else {
                        // invalid response
                        return $q.reject(response.data);
                    }

                }, function (response) {
                    // something went wrong
                    return $q.reject(response.data);
                });
        },
        
        addFeedback: function ( data, id) {
            // the $http API is based on the deferred/promise APIs exposed by the $q service
            // so it returns a promise for us by default
            return $http.post(apiUrl + "feedback/" , {"uid":id,"feedback_type":data.catlist,"subject":data.subject,"description":data.description})
                .then(function (response) {
                    if (typeof response.data === 'object') {

                        return response.data;

                    } else {
                        // invalid response
                        return $q.reject(response.data);
                    }

                }, function (response) {
                    // something went wrong
                    return $q.reject(response.data);
                });
        },
        activeMember: function (id) {
            // the $http API is based on the deferred/promise APIs exposed by the $q service
            // so it returns a promise for us by default
            return $http.post(apiUrl + "mainwing/active_membership/request" , {"user_id":id})
                .then(function (response) {
                    if (typeof response.data === 'object') {

                        return response.data;

                    } else {
                        // invalid response
                        return $q.reject(response.data);
                    }

                }, function (response) {
                    // something went wrong
                    return $q.reject(response.data);
                });
        },
            getActiveMemberStatus: function (id) {
            // the $http API is based on the deferred/promise APIs exposed by the $q service
            // so it returns a promise for us by default
            return $http.get(apiUrl + "active_member_status/"+id)
                .then(function (response) {
                    if (typeof response.data === 'object') {

                        return response.data;

                    } else {
                        // invalid response
                        return $q.reject(response.data);
                    }

                }, function (response) {
                    // something went wrong
                    return $q.reject(response.data);
                });
        },
        updateRoleById: function (id, data, permission) {
            // the $http API is based on the deferred/promise APIs exposed by the $q service
            // so it returns a promise for us by default
            return $http.post(apiUrl + "admin/roles/view/" + id, {"id":id,"name":data.role,"description":data.description,"permissions":permission})
                .then(function (response) {
                    if (typeof response.data === 'object') {

                        return response.data;

                    } else {
                        // invalid response
                        return $q.reject(response.data);
                    }

                }, function (response) {
                    // something went wrong
                    return $q.reject(response.data);
                });
        },
        getNewsResult: function () {
            // the $http API is based on the deferred/promise APIs exposed by the $q service
            // so it returns a promise for us by default
            return $http.get("http://vsktelangana.org/wp-json/wp/v2/posts/")
                .then(function (response) {
                    if (typeof response.data === 'object') {

                        return response.data;

                    } else {
                        // invalid response
                        return $q.reject(response.data);
                    }

                }, function (response) {
                    // something went wrong
                    return $q.reject(response.data);
                });
        },
        getChoices: function () {
            // the $http API is based on the deferred/promise APIs exposed by the $q service
            // so it returns a promise for us by default
            return $http.get(apiUrl + "admin/roles/choices")
                .then(function (response) {
                    if (typeof response.data === 'object') {

                        return response.data;

                    } else {
                        // invalid response
                        return $q.reject(response.data);
                    }

                }, function (response) {
                    // something went wrong
                    return $q.reject(response.data);
                });
        },
        addRoles: function (data,permission) {
            // the $http API is based on the deferred/promise APIs exposed by the $q service
            // so it returns a promise for us by default
            return $http.post(apiUrl + "admin/roles/add", { "name": data.role, "description": data.description, "permissions": permission })
            .then(function (response) {
                if (typeof response.data === 'object') {
                    return response;
                } else {
                    // invalid response
                    return $q.reject(response);
                }

            }, function (response) {
                // something went wrong
                return $q.reject(response);
            });
        },
        
        eventAttendanceInsert: function (uid,eid) {
            // the $http API is based on the deferred/promise APIs exposed by the $q service
            // so it returns a promise for us by default
            return $http.post(apiUrl + "event_attendance_insert/", { "uid": uid, "eid": eid })
            .then(function (response) {
                if (typeof response.data === 'object') {
                    return response;
                } else {
                    // invalid response
                    return $q.reject(response);
                }

            }, function (response) {
                // something went wrong
                return $q.reject(response);
            });
        },

        eventFeedbackInsert: function (uid,eid,data) {
            // the $http API is based on the deferred/promise APIs exposed by the $q service
            // so it returns a promise for us by default
            return $http.post(apiUrl + "membership_event_feedback/", { "uid": uid, "eid": eid, "description": data.description, "subject": data.subject })
            .then(function (response) {
                if (typeof response.data === 'object') {
                    return response;
                } else {
                    // invalid response
                    return $q.reject(response);
                }

            }, function (response) {
                // something went wrong
                return $q.reject(response);
            });
        },

 
        eventUploadPhotosInsert: function (uid,eid,files) {
            // the $http API is based on the deferred/promise APIs exposed by the $q service
            // so it returns a promise for us by default

            var da = { "uid": uid, "eid":eid,"files":files };
            
            return Upload.upload({
                url: apiUrl + "membership_event_upload_images/",
                data: da,
                enctype : "multipart/form-data",
                headers: {
                   "Content-Type": "multipart/form_data"
                }

            });
           
        },
        getBWReports: function (id) {
            // the $http API is based on the deferred/promise APIs exposed by the $q service
            // so it returns a promise for us by default
            return $http.get(apiUrl + "locations/membership/view/" + id)
                .then(function (response) {
                    if (typeof response.data === 'object') {

                        return response.data;

                    } else {
                        // invalid response
                        return $q.reject(response.data);
                    }

                }, function (response) {
                    // something went wrong
                    return $q.reject(response.data);
                });
        },

        getParentId: function (id) {
            // the $http API is based on the deferred/promise APIs exposed by the $q service
            // so it returns a promise for us by default
            return $http.get(apiUrl + "locations/parent/" + id)
                .then(function (response) {
                    if (typeof response.data === 'object') {

                        return response.data;

                    } else {
                        // invalid response
                        return $q.reject(response.data);
                    }

                }, function (response) {
                    // something went wrong
                    return $q.reject(response.data);
                });
        },

        getHierarchylevels: function () {
            // the $http API is based on the deferred/promise APIs exposed by the $q service
            // so it returns a promise for us by default
            return $http.get(apiUrl + "hierarchylevels/view/1")
                .then(function (response) {
                    if (typeof response.data === 'object') {

                        return response.data;

                    } else {
                        // invalid response
                        return $q.reject(response.data);
                    }

                }, function (response) {
                    // something went wrong
                    return $q.reject(response.data);
                });
        },
        getLoactions: function (sid) {
            // the $http API is based on the deferred/promise APIs exposed by the $q service
            // so it returns a promise for us by default
            return $http.get(apiUrl + "locations/view/1/" + sid)
                .then(function (response) {
                    if (typeof response.data === 'object') {

                        return response.data;

                    } else {
                        // invalid response
                        return $q.reject(response.data);
                    }

                }, function (response) {
                    // something went wrong
                    return $q.reject(response.data);
                });
        },
        getParliament: function () {
            // the $http API is based on the deferred/promise APIs exposed by the $q service
            // so it returns a promise for us by default
            return $http.get(apiUrl + "parliamentlist")
                .then(function (response) {
                    if (typeof response.data === 'object') {

                        return response.data;

                    } else {
                        // invalid response
                        return $q.reject(response.data);
                    }

                }, function (response) {
                    // something went wrong
                    return $q.reject(response.data);
                });
        },

        getDistricts: function (sid, did) {
            // the $http API is based on the deferred/promise APIs exposed by the $q service
            // so it returns a promise for us by default
            return $http.get(apiUrl + "locations/view/1/" + sid + "/" + did)
                .then(function (response) {
                    if (typeof response.data === 'object') {

                        return response.data;

                    } else {
                        // invalid response
                        return $q.reject(response.data);
                    }

                }, function (response) {
                    // something went wrong
                    return $q.reject(response.data);
                });
        },
        getAssembly: function (did, hid,hl_name) {
            // the $http API is based on the deferred/promise APIs exposed by the $q service
            // so it returns a promise for us by default
            return $http.get(apiUrl + "app/locations/" + hid + "/" + did+"/"+hl_name)
                .then(function (response) {
                    if (typeof response.data === 'object') {

                        return response.data;

                    } else {
                        // invalid response
                        return $q.reject(response.data);
                    }

                }, function (response) {
                    // something went wrong
                    return $q.reject(response.data);
                });
        },
        getMandals: function (did, hid,hl_name) {
            // the $http API is based on the deferred/promise APIs exposed by the $q service
            // so it returns a promise for us by default
            return $http.get(apiUrl + "app/locations/" + hid + "/" + did+"/"+hl_name)
                .then(function (response) {
                    if (typeof response.data === 'object') {

                        return response.data;

                    } else {
                        // invalid response
                        return $q.reject(response.data);
                    }

                }, function (response) {
                    // something went wrong
                    return $q.reject(response.data);
                });
        },
        getVillages: function (did, hid,hl_name) {
            // the $http API is based on the deferred/promise APIs exposed by the $q service
            // so it returns a promise for us by default
            return $http.get(apiUrl + "app/locations/" + hid + "/" + did+"/"+hl_name)
                .then(function (response) {
                    if (typeof response.data === 'object') {

                        return response.data;

                    } else {
                        // invalid response
                        return $q.reject(response.data);
                    }

                }, function (response) {
                    // something went wrong
                    return $q.reject(response.data);
                });
        },
        getBooths: function (did, hid,hl_name) {
            // the $http API is based on the deferred/promise APIs exposed by the $q service
            // so it returns a promise for us by default
            return $http.get(apiUrl + "app/locations/" + hid + "/" + did+"/"+hl_name)
                .then(function (response) {
                    if (typeof response.data === 'object') {

                        return response.data;

                    } else {
                        // invalid response
                        return $q.reject(response.data);
                    }

                }, function (response) {
                    // something went wrong
                    return $q.reject(response.data);
                });
        },
        getBoothNumber: function (loc_id) {
            // the $http API is based on the deferred/promise APIs exposed by the $q service
            // so it returns a promise for us by default
            return $http.get(apiUrl + "locations/buildbooth/" + loc_id)
                .then(function (response) {
                    if (typeof response.data === 'object') {

                        return response.data;

                    } else {
                        // invalid response
                        return $q.reject(response.data);
                    }

                }, function (response) {
                    // something went wrong
                    return $q.reject(response.data);
                });
        },
        getUserDetails: function (userid) {
            // the $http API is based on the deferred/promise APIs exposed by the $q service
            // so it returns a promise for us by default
            return $http.get(apiUrl + "membership/view/" + userid)
                .then(function (response) {
                    if (typeof response.data === 'object') {

                        return response.data;

                    } else {
                        // invalid response
                        return $q.reject(response.data);
                    }

                }, function (response) {
                    // something went wrong
                    return $q.reject(response.data);
                });
        },
        addMember: function (user,  f, boothcode) {
            var da = { "name": user.name, "surname": user.surname,"email":user.email,"membership_vehicle_status":user.vehicle, "gender": user.gender, "usercategory_id": user.caste, "mobile_number": parseInt(user.mobileNumber), "booth_id": boothcode, "age": user.age, "graduate": user.graduate, "idproof_id": user.idproof, "confirm_membership_id": user.confirmmembershipID, "confirm_mobile_number": user.confirm_mobile_number, "idproof_number": user.IDproofNumber, "membership_id": parseInt(user.membershipID),"pincode":user.pincode,"door_number":user.doorno,"street":user.street, "photo": f,"vehicle_no":user.vehicleno,"refered_by":user.refered_by,"smart_phone_status":user.smart_phone_status,"profession":user.profession };
            
            return Upload.upload({
                url: apiUrl + 'membership/add',
                data: da
                //headers: {
                //    "Content-Type": "application/json"
                //}

            });
            //console.log("test" + test);
            //return test;
        },
        updateMember: function (user, f, id, boothcode) {
            var da = { "uid": id, "name": user.name, "booth_id": boothcode, "email": user.email, "membership_vehicle_status": user.vehicle, "surname": user.surname, "mobile_number": parseInt(user.mobileNumber), "gender": user.gender, "usercategory_id": user.caste, "age": user.age, "idproof_id": user.idproof, "graduate": user.graduate, "confirm_membership_id": user.confirmmembershipID, "idproof_number": user.IDproofNumber, "confirm_mobile_number": user.confirm_mobile_number, "membership_id": parseInt(user.membershipID), "pincode": user.pincode, "door_number": user.doorno, "street": user.street, "photo": f, "vehicle_no": user.vehicleno,"whatsapp_number":user.whatsapp_number,"facebook":user.facebook_id,"twitter":user.twitter_id,"refered_by":user.refered_by,"smart_phone_status":user.smart_phone_status,"profession":user.profession };

            return Upload.upload({
                url: apiUrl + 'membership/update',
                data: da
                //headers: {
                //    "Content-Type": "application/json"
                //}

            });
            //console.log("test" + test);
            //return test;
        },
        isLogin: function (user) {
            // the $http API is based on the deferred/promise APIs exposed by the $q service
            // so it returns a promise for us by default
            return $http.post(apiUrl + "login", { "username": user.username, "password": user.password })
            .then(function (response) {
                if (typeof response.data === 'object') {
                    return response;
                } else {
                    // invalid response
                    return $q.reject(response);
                }

            }, function (response) {
                // something went wrong
                return $q.reject(response);
            });
        },
        changePassword: function (user, userid) {
            // the $http API is based on the deferred/promise APIs exposed by the $q service
            // so it returns a promise for us by default
            return $http.post(apiUrl + "membership/changepassword", { "uid": userid, "old_password": user.oldpassword, "new_password": user.newpassword })
            .then(function (response) {
                if (typeof response.data === 'object') {
                    return response;
                } else {
                    // invalid response
                    return $q.reject(response);
                }

            }, function (response) {
                // something went wrong
                return $q.reject(response);
            });
        },


        changeMobileNumber: function (user, userid) {
            // the $http API is based on the deferred/promise APIs exposed by the $q service
            // so it returns a promise for us by default
            return $http.post(apiUrl + "change_mobilenumber/" + userid, { "membership_id": user.membershipID, "confirm_membership_id": user.confirmmembershipID, "mobile_number": user.mobileNumber, "confirm_mobile_number": user.confirm_mobile_number })
            .then(function (response) {
                if (typeof response.data === 'object') {
                    return response;
                } else {
                    // invalid response
                    return $q.reject(response);
                }

            }, function (response) {
                // something went wrong
                return $q.reject(response);
            });
        },
        getUserMobile: function (user) {
            // the $http API is based on the deferred/promise APIs exposed by the $q service
            // so it returns a promise for us by default
            return $http.get(apiUrl + "change_mobilenumber/" + user)
            .then(function (response) {
                if (typeof response.data === 'object') {
                    return response;
                } else {
                    // invalid response
                    return $q.reject(response);
                }

            }, function (response) {
                // something went wrong
                return $q.reject(response);
            });
        },
        getUserVehicleStatus: function (user) {
            // the $http API is based on the deferred/promise APIs exposed by the $q service
            // so it returns a promise for us by default
            return $http.get(apiUrl + "uservehicle/")
            .then(function (response) {
                if (typeof response.data === 'object') {
                    return response;
                } else {
                    // invalid response
                    return $q.reject(response);
                }

            }, function (response) {
                // something went wrong
                return $q.reject(response);
            });
        },
        getVoterListByUserId: function (user,qtype) {
            // the $http API is based on the deferred/promise APIs exposed by the $q service
            // so it returns a promise for us by default
            return $http.get(apiUrl + "voterlist_by_userid/"+user+"/"+qtype)
            .then(function (response) {
                if (typeof response.data === 'object') {
                    return response;
                } else {
                    // invalid response
                    return $q.reject(response);
                }

            }, function (response) {
                // something went wrong
                return $q.reject(response);
            });
        },
        getPassword: function (user) {
            // the $http API is based on the deferred/promise APIs exposed by the $q service
            // so it returns a promise for us by default
            return $http.post(apiUrl + "forgotpassword", { "username": user.name })
            .then(function (response) {
                if (typeof response.data === 'object') {
                    return response;
                } else {
                    // invalid response
                    return $q.reject(response);
                }

            }, function (response) {
                // something went wrong
                return $q.reject(response);
            });
        },
        activeMembership: function (data,image,dob,receiptimage,uid) {
            // the $http API is based on the deferred/promise APIs exposed by the $q service
            // so it returns a promise for us by default

            var da = { "uid": uid,"f_s_name":data.f_s_name,"photo":image,"caste":data.caste,"education":data.education,"transaction_number":data.transaction_number,"receipt_image":receiptimage,"membership_since":data.member_since,"dob":dob,"door_number":data.doorno,"street":data.street,"pincode":data.pincode };
            
            return Upload.upload({
                url: apiUrl + "active_membership_mandatory_fileds",
                data: da
                //headers: {
                //    "Content-Type": "application/json"
                //}

            });
            
        },
        vistarakReport: function (data,user_id) {
            // the $http API is based on the deferred/promise APIs exposed by the $q service
            // so it returns a promise for us by default
            return $http.post(apiUrl +"vistarakreport/"+user_id,data)
            .then(function (response) {
            if (typeof response.data === 'object') {
           
            return response.data;
           
            } else {
            // invalid response
            return $q.reject(response.data);
            }
           
            }, function (response) {
            // something went wrong
            return $q.reject(response.data);
            });
            },


    };

}])

.service('BlankService', [function () {

}]);
