angular.module('app.controllers', [])

.controller('registrationCtrl', ['$scope', '$stateParams', 'BlankFactory', '$ionicPopup', '$ionicHistory', '$state', '$rootScope', '$localStorage', '$ionicLoading', '$http','$ionicModal',// The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, BlankFactory, $ionicPopup, $ionicHistory, $state, $rootScope, $localStorage, $ionicLoading,$http,$ionicModal) {
    //$scope.currentDate = new Date();
    // $scope.minDate = new Date(1989, 6, 1);
    // $scope.age="18 years"
    $rootScope.loginresult = 0;
    console.log($rootScope.loginresult);
    $scope.currentDate = new Date();
    $rootScope.timeValue = new Date();
    $rootScope.datetimeValue = new Date();
    $scope.rg = {
        name: "",
        surname: "",
        membershipID: "",
        confirmmembershipID: "",
        mobileNumber: "",
        boothNumber: "",
        idproof: "",
        IDproofNumber: "",
        selectedDate1: "",
        dob: "",
        gender: "",
        caste: "",
        age: "",
        confirm_mobile_number: "",
        graduate: false,
        upamandal:"",
        pincode:"",
        doorno:"",
        street:"",
        email:"",
        vehicle:"",
        vehicleno:"",
        profession:"",

        refered_by:"",
        smart_phone_status:false


    }
    $scope.pinFormat = /^[5][0-9]{5}$/;
    $scope.referralFormat = /^[6-9][0-9]{9}$/;
    // $scope.whatsappFormat = /^[6-9][0-9]{9}$/;
    $scope.emailFormat = /^(?!\.)[a-zA-Z0-9_,]*\.?[\.?a-zA-Z0-9_]+@[a-zA-Z]+\.(([a-zA-Z]{3})|([a-zA-Z]{2}\.[a-zA-Z]{2}))$/;
    $ionicModal.fromTemplateUrl('my-modal.html', {id: '1',
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal;
  });
  $ionicModal.fromTemplateUrl('my-pledge.html', {id: '2',
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal2 = modal;
  });
  $scope.openModal = function(index) {
    if (index == 1) $scope.modal2.show();
    else $scope.modal.show();
  };

  $scope.closeModal = function(index) {
    if (index == 1) $scope.modal2.hide();
    else $scope.modal.hide();
  };
  
    $scope.website = function () {
        window.open("https://188.42.96.92/", '_system');
    }
	$scope.callNumber = function(number){
		//alert(number)
		window.plugins.CallNumber.callNumber(onSuccess, onError, number, false);
	}
	function onSuccess(result){
  console.log("Success:"+result);
}

function onError(result) {
  console.log("Error:"+result);
}
    $scope.toknow_booth = function () {
		$scope.show()
        BlankFactory.getVoterListByUserId($scope.boothcodes,"booth").then(function(res){
            $scope.voterlist=res.data;
			$scope.hide();
             $scope.openModal(2)
        })

       

        /*window.open("http://ceoaperms.ap.gov.in/Search/search.aspx", '_system');*/
    }
    $scope.toknow_ur_booth = function(){
        window.open("http://ceoaperms.ap.gov.in/Search/search.aspx", '_system');
    }
    $scope.rg.gender = "Male";
    $scope.rg.age = 18;
    $scope.orig = angular.copy($scope.rg);
    $scope.reset = function () {

        $scope.rg = angular.copy($scope.orig);

        $scope.selectedDate1 = "";
        $scope.selectImage = 0;
        $scope.$destroy;
        //  $scope.MemberRegistration.$setPristine(true);
    }
    var date = new Date();
    $scope.show = function () {
        $ionicLoading.show({
            template: 'Loading...',

        }).then(function () {
            console.log("The loading indicator is now displayed");
        });
    };
    $scope.hide = function () {
        $ionicLoading.hide().then(function () {
            console.log("The loading indicator is now hidden");
        });
    };
    //$scope.getDate = function (data) {
    //    // alert();
    //    console.log(data);
    //    $scope.age = "Age: " + (date.getFullYear() - data.dob.getFullYear()) + " Years";
    //    console.log($scope.age);
    //}
    $scope.imageUpload = function (event) {

        var files = event.target.files; //FileList object
        var file = files[0];
        var reader = new FileReader();
        $scope.selectImage = 1;
        reader.onload = $scope.imageIsLoaded;
        reader.readAsDataURL(file);
        for (var i = 0; i < files.length; i++) {

        }
    }
    $scope.imageIsLoaded = function (e) {
        $scope.$apply(function () {
            $scope.stepsModel = (e.target.result);

            $scope.isImageUpload = 0;
        });
    }
    function getUserVehicleStatus(){
        BlankFactory.getUserVehicleStatus().then(function(res){
            $scope.vehicle=res.data;
        })
    };
    getUserVehicleStatus();
    function loadProofDetails() {
        BlankFactory.getIdProofDetails().then(function (res) {
            $scope.idproof = res;
        })
    }
    function loadCastDetails() {
        BlankFactory.getAllCast().then(function (res) {
            $scope.casts = res;
        })
    }
    function loadfunction() {
        $scope.reset();
    }
    $scope.vehiclenumber=0;
    $scope.getVehicle=function(){
        if($scope.rg.vehicle !=""){
            $scope.vehiclenumber=1;
        }
        else{
            $scope.vehiclenumber=0;
            $scope.rg.vehicleno="";
        }
        
    }
    $scope.clearConfirmVoterID = function () {
        if ($scope.rg.IDproofNumber.length == 7) {
            $scope.rg.confirmvoterid=""
        }
    }
    function loadParliament() {
        BlankFactory.getParliament().then(function (res) {
          
            $scope.parliament = res;
            $scope.showassembly = 0;
            $scope.showMandal = 0;
            $scope.showUpaMandal = 0;
            $scope.showBooth = 0;
            $scope.boothcode = $scope.boothcode + res[0].ec_code;
            $scope.hid = res[0].hierarchy_level;



        })
    }
    loadProofDetails();
    loadCastDetails();
    //loadhierarchylevels();
    loadParliament();
    loadfunction();
    $scope.showassembly = 0;
    $scope.showUpaMandal = 0;
    $scope.showBooth = 0;
    $scope.showBoothnumber = 0;
    $scope.showMandalup = 0;
    $scope.re = true;

    $scope.getAssembly = function (data) {
        if (data.ditrict != undefined) {
            BlankFactory.getAssembly(data.ditrict, "parliamentary","assembly").then(function (res) {
                var res = res.output_data;
                $scope.assemblies = res;
                $scope.rg.assembly="";
                $scope.showassembly = 1;
                $scope.showMandal = 0;
                $scope.showUpaMandal = 0;
                $scope.showBooth = 0;
                $scope.showBoothnumber = 0;
                $scope.showMandalup = 0;
                $scope.assemblieshid = res[0].hierarchy_level__name;
            })
        }
        else {
            $scope.showassembly = 0;
            $scope.showMandal = 0;
            $scope.showUpaMandal = 0;
            $scope.showBooth = 0;
            $scope.showMandalup = 0;
            $scope.showBoothnumber = 0;
        }
        
    }
    $scope.graduateresult = "No"
    $scope.getgraduate = function (data) {
        if (data.graduate == true) {
            $scope.graduateresult="Yes"
        }
        else {
            $scope.graduateresult = "No"
        }

    }
    $scope.getMandals = function (data) {
        if (data.assembly != undefined) {
            BlankFactory.getMandals(data.assembly, "assembly","mandal").then(function (res) {
                var res = res.output_data;
                  $scope.mandals = res;
                  $scope.rg.mandal = "";
                  $scope.showMandal = 1;
                  $scope.showUpaMandal = 0;
                  $scope.showBooth = 0;
                  $scope.showBoothnumber = 0;
                  $scope.mandalid = res[0].hierarchy_level__name;
        })

        }
        else {
           
            $scope.showMandal = 0;
            $scope.showUpaMandal = 0;
            $scope.showBooth = 0;
            $scope.showBoothnumber = 0;
            $scope.showMandalup = 0;
        }
       
    }

    $scope.getUpaMandals = function (data) {
        if (data.mandal != undefined) {
            BlankFactory.getMandals(data.mandal, $scope.mandalid).then(function (res) {
                var res1 = res.loc_data;
                $scope.upamandals = res1;
                $scope.villages = res.village_data;
                $scope.showUpaMandal = 1;
                $scope.rg.upamandal = "";
                 $scope.showMandalup = 1;
                 $scope.rg.village = "";
                $scope.showBooth = 0;
                $scope.showBoothnumber = 0;
                $scope.upamandalid = res1[0].hierarchy_level__name;
                $scope.villageid = res.village_data[0].hierarchy_level__name;
                $localStorage.village = res.village_data;
            })

        }
        else {
           
            $scope.showMandalup = 0;
            $scope.showUpaMandal = 0;
            $scope.showBooth = 0;
            $scope.showBoothnumber = 0;
        }

    }
    $scope.getVillages = function (data) {
        if (data.mandal != undefined && data.mandal != "") {
           
            BlankFactory.getVillages(data.mandal, "mandal","village").then(function (res) {
             var res = res.output_data;
                $scope.rg.village = "";
            $scope.villages = res;
            $scope.showUpaMandal = 1;
            $scope.showBooth = 0;
            $scope.showBoothnumber = 0;
            $scope.villageid = res[0].hierarchy_level__name;

            
        })
        }
        else {
            $scope.showUpaMandal = 0;
            $scope.showBooth = 0;
            $scope.showBoothnumber = 0;
            $scope.rg.village = "";
            $scope.re = true;
        }
       
    }
    $scope.getBooths = function (data) {
        if (data.village != undefined) {

            BlankFactory.getBooths(data.village, "village","booth").then(function (res) {
                var res = res.output_data;
                $scope.rg.booths="";
                $scope.booths = res;
                $scope.showBooth = 1;
                $scope.showBoothnumber = 0;
                $scope.boothcodes = data.village;
                
            })
        }
        else {
            $scope.showBooth = 0;
            $scope.showBoothnumber = 0;
        }
       
    }
    $scope.getBoothNumber = function (data) {
        if (data.booths != "") {

            BlankFactory.getBoothNumber(data.booths).then(function (res) {
            //var res = res.loc_data;
                $scope.rg.boothNumber = res.booth_code;
                $scope.boothcodes = data.booths;
                console.log($scope.boothcodes);
                $scope.showBoothnumber = 1;


            })
           
        }
        else {
             $scope.showBoothnumber = 0;
            BlankFactory.getBoothNumber($scope.rg.village).then(function (res) {
                $scope.rg.boothNumber = res.booth_code;
                $scope.boothcodes = $scope.rg.village;
                console.log($scope.boothcodes);
                $scope.showBoothnumber = 1;


            })
        }

    }

    $scope.getUserProfessionList = function(){
        BlankFactory.getUserProfessionList().then((function(res){
            if(res){
                $scope.profession_list = res
            }
        }))
    }
    $scope.getUserProfessionList();
    $scope.submit = function (data) {
        $scope.show();
           if (data.membershipID === data.confirmmembershipID && data.mobileNumber === data.confirm_mobile_number) {

                    if (data.IDproofNumber != "" && data.IDproofNumber.length < 19) {
                        if (data.IDproofNumber != data.confirmvoterid) {
                            $scope.hide();
                            var alertPopup = $ionicPopup.alert({
                                title: 'Registration',
                                cssClass: 'button-royal',
                                template: "Invalid Voter ID"

                            });
                        }
                        else {
                            finalcode();
                        }
                    }
                    else {
                        if (data.IDproofNumber != "") {
                            if (data.IDproofNumber != data.confirmvoterid) {
                                $scope.hide();
                                var alertPopup = $ionicPopup.alert({
                                    title: 'Registration',
                                    cssClass: 'button-royal',
                                    template: "Invalid Voter ID"

                                });
                            }
                            else {
                                finalcode();
                            }
                        }
                        else {
                            finalcode();
                        }

                    }


                }
                else {
                    $scope.hide();
                    var alertPopup = $ionicPopup.alert({
                        title: 'Registration',

                        template: "Mismatch Membership ID or Mobile Number, please re-enter."

                    });
                    alertPopup.then(function (res) {
                        if (res) {
                            $scope.myStyle = {
                                'background': "rgba(0,0,0,0.5) !important"
                            }
                            console.log($scope.myStyle);
                        }
                    })
                }



                function finalcode() {
                    var file = document.getElementById("file").files[0];
                    //var date = $scope.rg.dob.getFullYear() + "-" + ($scope.rg.dob.getMonth() + 1) + "-" + $scope.rg.dob.getDate();

                    BlankFactory.addMember(data, file, $scope.boothcodes).then(function (res) {


                        if (res.data.msg === "You are registered successfully") {
                            $scope.hide();
                            var alertPopup = $ionicPopup.alert({
                                title: 'Registration',
                                cssClass: 'button-royal',
                                template: res.data.msg + ", <br>" + "Dear " + res.data.name + ",<br>" + "Username :" + res.data.username + "<br>"+"Password sent to your registered Mobile No" 

                            });
                            alertPopup.then(function (res) {
                                if (res) {

                                    $scope.reset();
                                    $state.go("menu.home");
                                    $ionicHistory.nextViewOptions({
                                        disableBack: true
                                    });
                                }
                            })
                        }
                        if (res.data.mobile_number == "Mobile Number already registered") {
                            $scope.hide();
                            var alertPopup = $ionicPopup.alert({
                                title: 'Login',

                                template: res.data.mobile_number[0]
                            });

                        }
                        if (res.data.membership_id == "Membership Id already registered") {
                            $scope.hide();
                            var alertPopup = $ionicPopup.alert({
                                title: 'Login',

                                template: res.data.membership_id[0]
                            });

                        }
                        if (res.data.age == "Age should be minimum 18 Years old") {
                            $scope.hide();
                            var alertPopup = $ionicPopup.alert({
                                title: 'Registration',

                                template: res.data.age[0]
                            });


                        }
                         if (res.data.pincode == "Invalid Pincode") {
                            $scope.hide();
                            var alertPopup = $ionicPopup.alert({
                                title: 'Registration',

                                template: "Invalid PIN Code"
                            });

                        }
                         if(res.data.mobile_number=="Invalid mobile number")
                        {
                             $scope.hide();
                            var alertPopup = $ionicPopup.alert({
                                title: 'Registration',

                               template: "Mobile Number Should Start With (6/7/8/9) only"
                            });


                         }
                         if (res.data.voterid == "invalid voterid") {
                             $scope.hide();
                             var alertPopup = $ionicPopup.alert({
                                 title: 'Registration',

                                 template: "Please Enter Valid Voter ID"
                             });
                         }
                         if (res.data.vehicleno == "invalid vehicleno") {
                             $scope.hide();
                             var alertPopup = $ionicPopup.alert({
                                 title: 'Registration',

                                 template: "Please Enter Valid Vehicle Number"
                             });
                         }
                        if (res.data.idproof_number == "Id Proof Already Registered") {
                            $scope.hide();
                            var alertPopup = $ionicPopup.alert({
                                title: 'Registration',

                                template: res.data.idproof_number[0]
                            });

                        }
                    }, function (res) {
                        $scope.hide();
                    })
                }

        if (window.Connection) {
            //  alert("rathna" + window.Connection)
            if (navigator.connection.type == Connection.NONE) {

                var alertPopup = $ionicPopup.alert({
                    title: 'Internet',

                    template: "No Internet connection"
                });

            }
            else {
               
            }

        }
      

    }

    $scope.showidproof = 0;
    $scope.showprrofNumber = function (data) {
        if (data.idproof != "") {
            $scope.showidproof = 1;
            $scope.showerror = true;


        }
        else {

            $scope.showidproof = 0;
            $scope.rg.IDproofNumber = "";
            $scope.showerror = false;
        }
    }
    $scope.hideError = function () {
        $scope.showerror = false;
    }
}])
.controller('myaccountCtrl', ['$scope', '$stateParams', 'BlankFactory', '$ionicPopup', '$ionicHistory', '$state', '$localStorage', '$ionicLoading','$ionicModal',// The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, BlankFactory, $ionicPopup, $ionicHistory, $state, $localStorage, $ionicLoading,$ionicModal) {
    //$scope.currentDate = new Date();
    // $scope.minDate = new Date(1989, 6, 1);
    $scope.rg = {
        name: "",
        surname: "",
        membershipID: "",
        confirmmembershipID: "",
        mobileNumber: "",
        boothNumber: "",
        idproof: "",
        IDproofNumber: "",
        selectedDate1: "",
        dob: "",
        gender: "",
        caste: "",
        age: "",
        confirm_mobile_number: "",
        graduate: false,
        upamandal: "",
        confirmvoterid:"",
        pincode:"",
        doorno:"",
        street:"",
        email:"",
        vehicle:"",
        vehicleno:"",
        whatsapp_number:"",
        facebook_id:"",
        twitter_id:"",
        photo:"",
        profession:"",
        refered_by:"",
        smart_phone_status:false


    }
    $scope.showassembly = 1;
    $scope.showMandal = 1;
    $scope.showUpaMandal = 1;
    $scope.showBooth = 1;
    $scope.showMandalup = 1;
    $scope.showvoterid = 0;
    $scope.re = true;
    $scope.pinFormat = /^[5][0-9]{5}$/;
    $scope.referralFormat = /^[6-9][0-9]{9}$/;
    $scope.whatsappFormat = /^[6-9][0-9]{9}$/;
    $scope.emailFormat = /^(?!\.)[a-zA-Z0-9_,]*\.?[\.?a-zA-Z0-9_]+@[a-zA-Z]+\.(([a-zA-Z]{3})|([a-zA-Z]{2}\.[a-zA-Z]{2}))$/;
    $scope.show = function () {
        $ionicLoading.show({
            template: 'Loading...',

        }).then(function () {
            console.log("The loading indicator is now displayed");
        });
    };
    $scope.hide = function () {
        $ionicLoading.hide().then(function () {
            console.log("The loading indicator is now hidden");
        });
    };
    $scope.getUserProfessionList = function(){
        BlankFactory.getUserProfessionList().then((function(res){
            if(res){
                $scope.profession_list = res
            }
        }))
    }
    $scope.getUserProfessionList();
    $scope.toknow_booth = function () {
		$scope.show();
        
        BlankFactory.getVoterListByUserId($localStorage.userid,"userid").then(function(res){
			
            $scope.voterlist=res.data;
			$scope.hide();
             $scope.openModal(2)
        })
    }
     $scope.vehiclenumber=0;
    var date = new Date();
    $scope.orig = angular.copy($scope.rg);
    $scope.reset = function () {
        $scope.rg = angular.copy($scope.orig);
    }
    $scope.getMembershipID= function(){
        $scope.confirmmembershipID=1;
    }
    $scope.clearConfirmVoterID = function () {
        if ($scope.rg.IDproofNumber.length == 7) {
            $scope.rg.confirmvoterid = ""
        }
    }
    $ionicModal.fromTemplateUrl('my-modal.html', {id: '1',
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal;
  });
 
  $scope.openModal = function(index) {
    if (index == 1) $scope.modal2.show();
    else $scope.modal.show();
  };

  $scope.closeModal = function(index) {
    if (index == 1) $scope.modal2.hide();
    else $scope.modal.hide();
  };
    $scope.selectImage = 0;
    $scope.imageUpload = function (event) {

        var files = event.target.files; //FileList object
        var file = files[0];
        var reader = new FileReader();
        $scope.selectImage = 1;
        reader.onload = $scope.imageIsLoaded;
        reader.readAsDataURL(file);
        for (var i = 0; i < files.length; i++) {

        }
    }
    $scope.imageIsLoaded = function (e) {
        $scope.$apply(function () {
            $scope.stepsModel = (e.target.result);

            $scope.isImageUpload = 0;
        });
    }
    //$scope.details = $localStorage.userdetails;
    $scope.toknow_ur_booth = function(){
        window.open("http://ceoaperms.ap.gov.in/Search/search.aspx", '_system');
    }
    function loadProofDetails() {
        BlankFactory.getIdProofDetails().then(function (res) {
            $scope.idproof = res;
        })
    }
    function loadCastDetails() {
        BlankFactory.getAllCast().then(function (res) {
            $scope.casts = res;
        })
    }
    function getUserVehicleStatus(){
        BlankFactory.getUserVehicleStatus().then(function(res){
            $scope.vehicle=res.data;
        })
    };
    getUserVehicleStatus();
    function loadParliament() {
        BlankFactory.getParliament().then(function (res) {
            $scope.parliament = res;
            $scope.hid = res[0].hierarchy_level;



        })
    }
    $localStorage.village = "";
    $scope.getgraduate = function (data) {
        if (data.graduate == true) {
            $scope.graduateresult = "Yes"
        }
        else {
            $scope.graduateresult = "No"
        }

    }
    loadParliament();
    function loadData() {
        
        BlankFactory.getUserDetails($localStorage.userid).then(function (result) {
            $scope.hide();
           
            $scope.rg.name = result.name;
            if (result.graduate) {
                $scope.graduateresult = "Yes"
            }
            else {
                $scope.graduateresult = "No"
            }

            $scope.rg.graduate = result.graduate;
            $scope.rg.surname = result.surname;
            if(result.membership_id !="")
            {
                $scope.confirmmembershipID=0;
                $scope.rg.membershipID = result.membership_id;
            } 
            $scope.rg.mobileNumber = result.mobile_number;
            $localStorage.parliament = result.parliamentary[0];
            $localStorage.assembley = result.assembly[0];
            $localStorage.mandal = result.mandal[0];
            //$localStorage.upamandals = result.shaktikendra[0];
            $localStorage.upamandal = result.village[0];
            $scope.rg.pincode=result.pincode;
            $scope.rg.doorno=result.door_number;
            $scope.rg.street=result.street;
             $scope.rg.whatsapp_number = result.whatsapp_number;
            $scope.rg.facebook_id = result.facebook;
            $scope.rg.twitter_id = result.twitter;
            $scope.rg.refered_by = result.refered_by;
            if (result.booth_id != "") {
                $scope.showBoothnumber = 1;
            }
            else {
                $scope.showBoothnumber = 0;
            }
            $scope.rg.boothNumber = result.booth_id;
            $localStorage.boothid = result.booth_id;
            $scope.rg.ditrict = result.parliamentary[1].toString();
            $scope.rg.gender = result.gender;
            $scope.rg.age = result.age;
            $scope.hide();
            BlankFactory.getAssembly(result.parliamentary[1],"parliamentary","assembly").then(function (res) {
                $scope.assemblies = res.output_data;
                $scope.rg.assembly = result.assembly[1].toString();
				BlankFactory.getMandals( result.assembly[1],"assembly","mandal").then(function (res) {
					$scope.mandals = res.output_data;
					$scope.rg.mandal = result.mandal[1].toString();
					BlankFactory.getVillages( result.mandal[1],"mandal","village").then(function (res) {
						$scope.villages = res.output_data;						
						$scope.rg.village = result.village[1].toString();
						BlankFactory.getBooths( result.village[1],"village","booth").then(function (res) {
							$scope.booths = res.output_data;
							$scope.rg.booths = result.booth[1].toString();


						})
					})

				})
            })


           
            
            if (result.idproof_number != "") {
                $scope.showidproofer = 0;
            }
            else {
                $scope.showidproofer = 0;
            }
            $scope.rg.IDproofNumber = result.idproof_number;

            $scope.src = result.photo;
            $scope.rg.gender = result.gender;
            $scope.rg.email = result.email;
            $scope.rg.vehicle = result.membership_vehicle_status.toString();
            if (result.membership_vehicle_status != "")
            {
                $scope.vehiclenumber=1;
                $scope.rg.vehicleno=result.vehicle_no;
            }
           
            $scope.rg.caste = result.usercategory_id.toString();
            if(result.smart_phone_status =='true'){
                $scope.rg.smart_phone_status=true
            }
            else{
                $scope.rg.smart_phone_status=false;
            }
            
            if(result.profession!=""){
                $scope.rg.profession = result.profession.toString();
            }
            




            

        }, function (res) {

            $scope.hide();
        })
    }
       
    loadProofDetails();
    loadCastDetails();
    loadData();
    $scope.getVehicle=function(){
        if($scope.rg.vehicle !=""){
            $scope.vehiclenumber=1;
        }
        else{
            $scope.vehiclenumber=0;
            
        }
        
    }
    $scope.showconfirm = 0;
    $scope.showmember = 0;
    $scope.showidproofer = 0;
    $scope.editMembershipID = function (data) {
        if (data.IDproofNumber.length >=9 && data.IDproofNumber.length >0) {
            $scope.showidproofer = 1;
        }
        else {
            
                $scope.showidproofer = 0;
                $scope.rg.confirmvoterid="";
           
        }

    }
    $scope.getAssembly = function (data) {
        if (data.ditrict != undefined) {
            BlankFactory.getAssembly(data.ditrict, "parliamentary","assembly").then(function (res) {
                var res = res.output_data;
                $scope.assemblies = res;
                $scope.rg.assembly="";
                $scope.showassembly = 1;
                $scope.showMandal = 0;
                $scope.showUpaMandal = 0;
                $scope.showBooth = 0;
                $scope.showBoothnumber = 0;
                $scope.showMandalup = 0;
                $scope.assemblieshid = res[0].hierarchy_level__name;
            })
        }
        else {
            $scope.showassembly = 0;
            $scope.showMandal = 0;
            $scope.showUpaMandal = 0;
            $scope.showBooth = 0;
            $scope.showMandalup = 0;
            $scope.showBoothnumber = 0;
        }
        
    }
    $scope.graduateresult = "No"
    $scope.getgraduate = function (data) {
        if (data.graduate == true) {
            $scope.graduateresult="Yes"
        }
        else {
            $scope.graduateresult = "No"
        }

    }
    $scope.getMandals = function (data) {
        if (data.assembly != undefined) {
            BlankFactory.getMandals(data.assembly, "assembly","mandal").then(function (res) {
                var res = res.output_data;
                  $scope.mandals = res;
                  $scope.rg.mandal = "";
                  $scope.showMandal = 1;
                  $scope.showUpaMandal = 0;
                  $scope.showBooth = 0;
                  $scope.showBoothnumber = 0;
                  $scope.mandalid = res[0].hierarchy_level__name;
        })

        }
        else {
           
            $scope.showMandal = 0;
            $scope.showUpaMandal = 0;
            $scope.showBooth = 0;
            $scope.showBoothnumber = 0;
            $scope.showMandalup = 0;
        }
       
    }

    $scope.getUpaMandals = function (data) {
        if (data.mandal != undefined) {
            BlankFactory.getMandals(data.mandal, $scope.mandalid).then(function (res) {
                var res1 = res.loc_data;
                $scope.upamandals = res1;
                $scope.villages = res.village_data;
                $scope.showUpaMandal = 1;
                $scope.rg.upamandal = "";
                 $scope.showMandalup = 1;
                 $scope.rg.village = "";
                $scope.showBooth = 0;
                $scope.showBoothnumber = 0;
                $scope.upamandalid = res1[0].hierarchy_level__name;
                $scope.villageid = res.village_data[0].hierarchy_level__name;
                $localStorage.village = res.village_data;
            })

        }
        else {
           
            $scope.showMandalup = 0;
            $scope.showUpaMandal = 0;
            $scope.showBooth = 0;
            $scope.showBoothnumber = 0;
        }

    }
    $scope.getVillages = function (data) {
        if (data.mandal != undefined && data.mandal != "") {
           
            BlankFactory.getVillages(data.mandal, "mandal","village").then(function (res) {
             var res = res.output_data;
                $scope.rg.village = "";
            $scope.villages = res;
            $scope.showUpaMandal = 1;
            $scope.showBooth = 0;
            $scope.showBoothnumber = 0;
            $scope.villageid = res[0].hierarchy_level__name;

            
        })
        }
        else {
            $scope.showUpaMandal = 0;
            $scope.showBooth = 0;
            $scope.showBoothnumber = 0;
            $scope.rg.village = "";
            $scope.re = true;
        }
       
    }
    $scope.getBooths = function (data) {
        if (data.village != undefined) {

            BlankFactory.getBooths(data.village,"village","booth").then(function (res) {
                var res = res.output_data;
                $scope.rg.booths="";
                $scope.booths = res;
                $scope.showBooth = 1;
                $scope.showBoothnumber = 0;
                $scope.boothcodes = data.village;
                
            })
        }
        else {
            $scope.showBooth = 0;
            $scope.showBoothnumber = 0;
        }
       
    }
    $scope.getBoothNumber = function (data) {
        if (data.booths != "") {
            BlankFactory.getBoothNumber(data.booths).then(function (res) {
                
                $scope.rg.boothNumber = res.booth_code;
                $scope.boothcodes = data.booths;
                console.log($scope.boothcodes);
                $scope.showBoothnumber = 1;


            })

        }
        else {
            $scope.showBoothnumber = 0;
            BlankFactory.getBoothNumber($scope.rg.village).then(function (res) {
               
                $scope.rg.boothNumber = res.booth_code;
                $scope.boothcodes = $scope.rg.village;
                console.log($scope.boothcodes);
                $scope.showBoothnumber = 1;


            })
        }

    }
   
    $scope.submit = function (data) {
        $scope.show();
          
          if($scope.confirmmembershipID ==1){
            if($scope.rg.membershipID== $scope.rg.confirmmembershipID){
                checkcode();
            }
            else{
                            $scope.hide();
                            var alertPopup = $ionicPopup.alert({
                                title: 'My Profile',
                                cssClass: 'button-royal',
                                template: "Membership ID missmatch"

                            });
            }
          }
          else
          {
            checkcode()
          }


                function checkcode() {
                    if($scope.showidproofer==0){
                        checkinner();
                    }
                    else{
                        if (data.IDproofNumber != data.confirmvoterid) {
                            $scope.hide();
                            var alertPopup = $ionicPopup.alert({
                                title: 'My Profile',
                                cssClass: 'button-royal',
                                template: "Invalid Voter ID"

                            });
                        }
                        else {

                            checkinner();

                        }
                    }

                    /*if (data.IDproofNumber != "" && data.IDproofNumber.length < 18) {
                        if (data.IDproofNumber != data.confirmvoterid) {
                            $scope.hide();
                            var alertPopup = $ionicPopup.alert({
                                title: 'My Profile',
                                cssClass: 'button-royal',
                                template: "Invalid Voter ID"

                            });
                        }
                        else {

                            checkinner();

                        }
                    }
                    else if (data.IDproofNumber != "" && $scope.rg.confirmvoterid.length > 18 && data.IDproofNumber.length > 18) {
                        if (data.IDproofNumber != data.confirmvoterid) {
                            $scope.hide();
                            var alertPopup = $ionicPopup.alert({
                                title: 'My Profile',
                                cssClass: 'button-royal',
                                template: "Invalid Voter ID"

                            });
                        }
                        else {
                            checkinner();
                        }
                    }
                    else {
                        checkinner();
                    }*/

                    
                }
                function checkinner() {

                    var file = document.getElementById("file").files[0];
                    //var date = $scope.rg.dob.getFullYear() + "-" + ($scope.rg.dob.getMonth() + 1) + "-" + $scope.rg.dob.getDate();
                    if (data.vehicleno == null) {
                        data.vehicleno = "";
                    }
                    BlankFactory.updateMember(data, file, $localStorage.userid, $scope.boothcodes).then(function (res) {
                        //alert(res.date_of_birth[0]);
                        if (res.data.msg === "Your profile have been updated") {
                            $scope.hide();
                            var alertPopup = $ionicPopup.alert({
                                title: 'My Profile',
                                cssClass: 'button-royal',
                                template: "Your Profile updated successfully"

                            });
                            alertPopup.then(function (res) {
                                if (res) {
                                    $state.go("menu.homeforlogin");

                                    $ionicHistory.nextViewOptions({
                                        disableBack: true
                                    });
                                }
                            })
                        }
                        if (res.data.age == "Age should be minimum 18 Years old") {
                            $scope.hide();
                            var alertPopup = $ionicPopup.alert({
                                title: 'My Profile',

                                template: res.data.age[0]
                            });

                        }
                        if (res.data.voterid == "invalid voterid") {
                            $scope.hide();
                            var alertPopup = $ionicPopup.alert({
                                title: 'My Profile',

                                template: "Please Enter Valid Voter ID"
                            });
                        }
                        if (res.data.vehicleno == "invalid vehicleno") {
                            $scope.hide();
                            var alertPopup = $ionicPopup.alert({
                                title: 'My Profile',

                                template: "Please Enter Valid Vehicle Number"
                            });
                        }

                        if(res.data.non_field_errors=="MembershipID already exist")
                        {
                             $scope.hide();
                            var alertPopup = $ionicPopup.alert({
                                title: 'My Profile',

                                template: "MembershipID already exist"
                            });


                        }
                         if(res.data.non_field_errors=="Invalid mobile number")
                        {
                             $scope.hide();
                            var alertPopup = $ionicPopup.alert({
                                title: 'My Profile',

                                template: "Invalid mobile number"
                            });


                        }
                        if (res.data.pincode == "Invalid Pincode") {
                            $scope.hide();
                            var alertPopup = $ionicPopup.alert({
                                title: 'My Profile',

                                template: "Invalid PIN Code"
                            });

                        }
                        if (res.data.idproof_number == "Id Proof Already Registered") {
                            $scope.hide();
                            var alertPopup = $ionicPopup.alert({
                                title: 'Registration',

                                template: res.data.idproof_number[0]
                            });

                        }
                    }, function (res) {

                        $scope.hide();
                    })
                }
        
               
             
        
        if (window.Connection) {
            //  alert("rathna" + window.Connection)
            if (navigator.connection.type == Connection.NONE) {
                $scope.hide();
                var alertPopup = $ionicPopup.alert({
                    title: 'Internet',

                    template: "No Internet connection"
                });

            }
            else {
              
            }
        }




    }

    $scope.confirmMember = function () {
        if ($scope.rg.membershipID === $scope.rg.confirmmembershipID) {

        }
        else {
            var alertPopup = $ionicPopup.alert({
                title: 'Membership Number',
                cssClass: 'button-royal',
                buttons: [

            {
                text: '<b>OK</b>',
                type: 'button-royal',

            }
                ],
                template: "Wrong membership Number"
            });
        }



    }
    $scope.editvoter = function () {
        $scope.showidproofer = 0;
    }
    $scope.showprrofNumber = function (data) {
        if (data.idproof != "") {
            $scope.showidproof = 1;
            $scope.showerror = true;


        }
        else {

            $scope.showidproof = 0;
            $scope.rg.IDproofNumber = "";
        }
    }
    $scope.hideError = function () {
        $scope.showerror = false;
    }
}])
.controller('cartCtrl', ['$scope', '$stateParams', '$state', 'BlankFactory', '$rootScope', '$location', '$ionicPopup', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $state, BlankFactory, $rootScope, $location, $ionicPopup) {


    BlankFactory.getNews().then(function (res) {
        $rootScope.newsData = res.items;
        // $state.go("menu.events");

    }, function (msg) {
        //alert();
        // $state.go("menu.events");
    })
    $scope.getdata = function (data) {
        window.location.href = data;
        //  alert("hi")
        if (window.Connection) {
            //  alert("rathna" + window.Connection)
            if (navigator.connection.type == Connection.NONE) {

                var alertPopup = $ionicPopup.alert({
                    title: 'Internet',

                    template: "No Internet connection"
                });

            }
            else {
                window.location.href = data;
            }

        }


    }

}])

.controller('loginCtrl', ['$scope', '$stateParams', 'BlankFactory', '$ionicPopup', '$ionicHistory', '$state', '$localStorage', '$rootScope',// The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, BlankFactory, $ionicPopup, $ionicHistory, $state, $localStorage, $rootScope) {
    $localStorage.userid = 0;
    $scope.home = function () {
        $state.go("menu.home");
        $ionicHistory.nextViewOptions({
            disableBack: true
        });
    }
    $scope.user = {
        username: "",
        password: "",

    };
    $scope.orig = angular.copy($scope.user);
    $scope.reset = function () {
        $scope.user = angular.copy($scope.orig);
    }
    $scope.login = function (data) {
        $localStorage.password = $scope.user.password;
        if ($scope.user.username == "" || $scope.user.password == "") {

            var alertPopup = $ionicPopup.alert({
                title: 'Login',

                template: "Please enter Login Credentials"
            });
        }
        else {
            BlankFactory.isLogin(data).then(function (res) {
                console.log(res)
                if (res.data.msg === "Invalid Username/Password") {
                    var alertPopup = $ionicPopup.alert({
                        title: 'Login',

                        template: res.data.msg
                    });

                }

                else {

                    if (res.data.group=="membership") {

                        $localStorage.userid = res.data.user_id;
                        $localStorage.loginresults = res.data.group;
                        $localStorage.booth_name = res.data.booth_name;
                        $localStorage.village_name = res.data.village_name
                        $localStorage.role = res.data.role
                        //window.localStorage.setItem( 'role', res.data.role );
                        //BlankFactory.setUserRole(res.data.role);
                        $rootScope.loginresult = $localStorage.loginresults;
                        //$rootScope.role = res.data.role;

                        console.log("logintCtrl--" + $localStorage.loginresults)
                        $scope.reset();
                        $state.go("menu.homeforlogin",{obj: res.data.role});
                       // $state.go("menu.homeforlogin",);
                        $ionicHistory.nextViewOptions({
                            disableBack: true
                        });
                    }
                    if (res.data.group === "administration") {

                        $localStorage.userid = res.data.user_id;
                        $localStorage.loginresults = res.data.group;
                        $rootScope.loginresult = $localStorage.loginresults;

                        console.log("logintCtrl--" + $localStorage.loginresults);
                        $state.go("menu.superadmin");
                        $ionicHistory.nextViewOptions({
                            disableBack: true
                        });
                        $scope.reset();
                    }


                    console.log($localStorage.userid);


                    //  $rootScope.loginresult = true;
                    //var alertPopup = $ionicPopup.alert({
                    //    title: 'Membership Number',

                    //    template: "Login Successfully"
                    //});
                    //alertPopup.then(function (res) {
                    //    if (res) {

                    //    }
                    //})
                }

            })
        }
    }
        if (window.Connection) {
            //  alert("rathna" + window.Connection)
            if (navigator.connection.type == Connection.NONE) {

                var alertPopup = $ionicPopup.alert({
                    title: 'Internet',

                    template: "No Internet connection"
                });

            }
            else {
                 

        }

    }
    $scope.join = function () {
        $ionicHistory.clearCache().then(function () { $state.go('menu.registration') });
        $ionicHistory.nextViewOptions({
            disableBack: true
        });
    }
    $scope.forgotpage = function () {
        $state.go("menu.forgotpassword");
        $ionicHistory.nextViewOptions({
            disableBack: true
        });
    }
}])
.controller('forgotpasswordCtrl', ['$scope', '$stateParams', 'BlankFactory', '$ionicPopup', '$ionicHistory', '$state', '$ionicLoading',// The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, BlankFactory, $ionicPopup, $ionicHistory, $state, $ionicLoading) {
    $scope.show = function () {
        $ionicLoading.show({
            template: 'Loading...',
            duration: 3000
        }).then(function () {
            console.log("The loading indicator is now displayed");
        });
    };
    $scope.hide = function () {
        $ionicLoading.hide().then(function () {
            console.log("The loading indicator is now hidden");
        });
    };
    $scope.submit = function (data) {
        $scope.show();
        BlankFactory.getPassword(data).then(function (res) {
                    if (res.data.msg === "Invalid Username") {
                        $scope.hide();
                        var alertPopup = $ionicPopup.alert({
                            title: 'Forgot password',

                            template: res.data.msg
                        });
                    }
                    if (res.data.msg === "Success") {
                        $scope.hide();
                        var alertPopup = $ionicPopup.alert({
                            title: 'Forgot password',

                            template: "New Password sent to your registered Mobile No" + "<br>" + "Username :" + res.data.username 
                        });
                        alertPopup.then(function (res) {
                            if (res) {
                                $state.go("menu.home");
                            }
                        })
                    }

                })
        if (window.Connection) {
            //  alert("rathna" + window.Connection)
            if (navigator.connection.type == Connection.NONE) {
                $scope.hide();
                var alertPopup = $ionicPopup.alert({
                    title: 'Internet',

                    template: "No Internet connection"
                });

            }
            else {
               
            }

        }



    }

}])
.controller('managerolesCtrl', ['$scope', '$stateParams', 'BlankFactory', '$ionicPopup', '$ionicHistory', '$state', '$ionicModal',// The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, BlankFactory, $ionicPopup, $ionicHistory, $state, $ionicModal) {
    $scope.modal = $ionicModal.fromTemplateUrl('modal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function (modal) {
        $scope.modal = modal;
    });
    $scope.openModal = function () {

        $scope.modal.show();
        // console.log(a)

    };
    $scope.closeModal = function () {
        $scope.modal.hide();
    };
    $scope.rg = {
        role: "",
        description: ""
    }

    // Cleanup the modal when we're done with it!
    $scope.$on('$destroy', function () {
        $scope.modal.remove();
    });
    // Execute action on hide modal
    $scope.$on('modal.hidden', function () {
        // Execute action
    });

    $scope.$on('modal.removed', function () {
        // Execute action
    });
    $scope.modelpopup = function (data) {
        BlankFactory.getRoleById(data).then(function (res) {
            $scope.editlist = res;
            $scope.id = res.id;
            $scope.rg.role = res.name;
            $scope.rg.description = res.description;
            for (var i = 0; i < res.permissions.length; i++) {
                if (res.permissions.length > 0) {
                    $scope.order[res.permissions[i]] = true;
                    $scope.format();
                }
            }

        })

        $scope.openModal();
        // $scope.lists = data;

    }

    $scope.data = [];

    $scope.isChecked = false;
    $scope.selected = [];
    $scope.checkedOrNot = function (asset, isChecked, index) {
        if (isChecked) {
            $scope.selected.push(asset);
        } else {
            var _index = $scope.selected.indexOf(asset);
            $scope.selected.splice(_index, 1);
        }
    };
    $scope.order = {};
    $scope.format = function () {
        $scope.modifiedOrder = [];
        angular.forEach($scope.order, function (value, key) {
            if (value) {
                console.log(value);
                $scope.modifiedOrder.push(parseInt(key));
            }
        });
    }
    $scope.submit = function (res) {
        BlankFactory.addRoles(res, $scope.modifiedOrder).then(function (res) {
            if (res.data.msg == "Your Role Added successfully") {
                loadroles();
                $scope.closeModal();
            }


        })
    }
    function loadroles() {
        BlankFactory.getAllRoles().then(function (res) {
            $scope.roles = res;
        })
    }
    function loadnews() {
        BlankFactory.getNewsResult().then(function (res) {
            $scope.html = res;
        })
    }
    loadnews();
    loadroles();
    function loadchoices() {
        BlankFactory.getChoices().then(function (res) {
            $scope.assets = res;
        })
    }
    loadchoices();

    $scope.addRole = function () {
        $scope.form = "add";
        $scope.modal.show();
    }
    $scope.update = function (data) {
        BlankFactory.updateRoleById($scope.id, data, $scope.modifiedOrder).then(function (res) {
            if (res.msg == "Your Role Updated successfully") {
                loadroles();
                $scope.modal.hide();
            }

        })
    }
    $scope.delete = function (data) {
        var popup = $ionicModal.alert({

        })

        BlankFactory.deleRoleById(data).then(function () {
            var alertPopup = $ionicPopup.conform({
                title: 'Roles',

                template: res.data.msg
            });
        })
    }
}])
.controller('changepasswordCtrl', ['$scope', '$stateParams', 'BlankFactory', '$ionicPopup', '$ionicHistory', '$localStorage', '$state', '$ionicLoading',// The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, BlankFactory, $ionicPopup, $ionicHistory, $localStorage, $state, $ionicLoading) {
    $scope.show = function () {
        $ionicLoading.show({
            template: 'Loading...',
            duration: 3000
        }).then(function () {
            console.log("The loading indicator is now displayed");
        });
    };
    $scope.hide = function () {
        $ionicLoading.hide().then(function () {
            console.log("The loading indicator is now hidden");
        });
    };
    $scope.rg = {

        oldpassword: "",
        newpassword: "",
        confirmpassword: ""
    }
    $scope.orig = angular.copy($scope.rg);
    $scope.reset = function () {
        $scope.rg = angular.copy($scope.orig);
    }
    $scope.submit = function (data) {
        $scope.show();
         if (data.newpassword === data.confirmpassword) {
                    BlankFactory.changePassword(data, $localStorage.userid).then(function (res) {
                        if (res.data.msg === "Invalid Password") {
                            $scope.hide();
                            var alertPopup = $ionicPopup.alert({
                                title: 'Change Password',

                                template: res.data.msg
                            });
                        }
                        else if (res.data.msg === "Invalid Old Password") {
                            $scope.hide();
                            var alertPopup = $ionicPopup.alert({
                                title: 'Change Password',

                                template: res.data.msg
                            });
                        }
                        else {
                            $scope.hide();
                            var alertPopup = $ionicPopup.alert({
                                title: 'Change Password',

                                template: "Your Password changed successfully"
                            });
                            alertPopup.then(function (res) {
                                $state.go("menu.homeforlogin");
                                $scope.reset();
                            })

                        }

                    })
                }
                else {
                    var alertPopup = $ionicPopup.alert({
                        title: 'Change Password',

                        template: "Password does not match"
                    });
                }
        if (window.Connection) {
            //  alert("rathna" + window.Connection)
            if (navigator.connection.type == Connection.NONE) {
                $scope.hide();
                var alertPopup = $ionicPopup.alert({
                    title: 'Internet',

                    template: "No Internet connection"
                });

            }
            else {
              
            }

        }



    }

}])
 .controller('homeforloginCtrl', ['$scope', '$stateParams', 'BlankFactory', '$ionicPopup', '$ionicHistory', '$localStorage', '$state', '$rootScope', '$http', '$location', '$cordovaBarcodeScanner','$ionicLoading',// The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, BlankFactory, $ionicPopup, $ionicHistory, $localStorage, $state, $rootScope, $http, $location, $cordovaBarcodeScanner,$ionicLoading) {
    //alert($location.port())
// var url = "http://10.10.11.233:8000"
    $scope.show = function () {
        $ionicLoading.show({
            template: 'Loading...',
            duration: 3000
        }).then(function () {
            console.log("The loading indicator is now displayed");
        });
    };
    $scope.hide = function () {
        $ionicLoading.hide().then(function () {
            console.log("The loading indicator is now hidden");
        });
    };
    $rootScope.loginresult = $localStorage.loginresults;
    console.log("homeforloginCtrl........" + $rootScope.loginresult)
    $scope.isVisibleVistarak=0;
    //$scope.role=BlankFactory.getUserRole();
    //$scope.role = window.localStorage.getItem('role' );
    //console.log($rootScope.role)
    // if($rootScope.role=='vistarak'){
    //     $scope.isVisibleVistarak=1
    // }
    //console,log($state.params.obj+" "+"jhkjdh")

    if ($state.params.obj=='vistarak'){
        $scope.isVisibleVistarak=1
    }
    else{
        if($localStorage.role=='vistarak'){
            $scope.isVisibleVistarak=1
        }
        else{
            
        }
    }
    $scope.login = function (data) {
        $state.go("menu.changepassword");
        //BlankFactory.changePassword(data,$localStorage).success(function (res) {
        //    if (res) {

        //    }

        //})

    }
    $scope.village_name = $localStorage.village_name;
    console.log($localStorage.village_name)
    $scope.getMemberdetails = function () {
        $ionicHistory.clearCache().then(function () { $state.go("menu.myaccount"); });

    }
    $scope.scan = function () {
        $cordovaBarcodeScanner
      .scan()
      .then(function (result) {
          var data = JSON.parse(result.text)
          var alertPopup = $ionicPopup.alert({
              title: 'QR Scan',

              template: "<strong>Name</strong> : " + data["Name"] + "<br>" + "<strong>Membership ID</strong> : " + data['membershi_id'] + "<br>" + "<strong>Mobile Number</strong> : " + data['mobile_number'] + "<br>" + "<strong>Address</strong> : " + data['Address'] + "<br>" + "<strong>Age</strong> : " + data['Age'] + "<br>" + "<strong>Caste</strong> : " + data["Caste"] + "<br>" + "<strong>Gender</strong> : " + data['Gender']
          });
          
          //alert("We got a barcode\n" +
          //       "Result: " + result.text + "\n" +
          //       "Format: " + result.format + "\n" +
          //       "Cancelled: " + result.cancelled);
          console.log(result)
      }, function (error) {
          // An error occurred
      });
    }
    $scope.getNews = function () {
        BlankFactory.getNews().then(function (res) {
            $rootScope.newsData = res.items;

            $state.go("menu.cart");

        }, function (msg) {
            //alert();
            $state.go("menu.cart");
        })
    }
    $scope.url=BlankFactory.getUrl()
    $scope.getNews2 = function () {
        BlankFactory.getNews().then(function (res) {
            $rootScope.newsData = res.items;
            $state.go("menu.events");

        }, function (msg) {
            //alert();
            $state.go("menu.cart");
        })
    }
    $scope.getfeedback= function(){
        $state.go("menu.feedback");
    }
    $scope.activeMember = function(){
        $state.go("menu.activemebership");
    }
    $scope.getPDF=function(){
        window.open($scope.url+"accounts/membership/idcard/pdf/" + $localStorage.userid, '_system');
    }
   
    $scope.gotoWebsite = function () {
        BlankFactory.getwebsite($localStorage.userid, $localStorage.password).then(function (res) {
            if (res == "urlmsg") {
                window.open($scope.url, '_system');

            }

        })
    }
}])
.controller('homeCtrl', ['$scope', '$stateParams', '$state', '$ionicHistory', '$localStorage', '$rootScope', 'BlankFactory', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $state, $ionicHistory, $localStorage, $rootScope, BlankFactory) {
    $rootScope.loginresult = 0;
    console.log($rootScope.loginresult);
    $scope.addMember = function () {
        $ionicHistory.clearCache().then(function () { $state.go('menu.registration') });

        $ionicHistory.nextViewOptions({
            disableBack: true
        });
    }

    $scope.getNews = function () {
        $state.go("menu.cart");
    }

    $scope.dhanushwebsite = function () {
        window.open("http://www.dhanushinfotech.com/Pages/index.htm", '_system');
    }
    $scope.login = function () {
        $state.go("menu.login");
    }
    // 
    //console.log("homectrllocal---" + $localStorage.userid);
    //$rootScope.loginresult = $localStorage.loginresults;
    //console.log($localStorage.loginresults+"localstorage")
    //console.log("homectrl" + $rootScope.loginresult);
}])
.controller('reportsCtrl', ['$scope', '$stateParams', '$state', '$ionicHistory', '$localStorage', '$rootScope', 'BlankFactory', '$ionicModal', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $state, $ionicHistory, $localStorage, $rootScope, BlankFactory, $ionicModal) {
    function loadhierarchylevels() {
        BlankFactory.getHierarchylevels().then(function (res) {
            $scope.comaignid = res[1].id;
            BlankFactory.getLoactions($scope.comaignid).then(function (res) {
                $scope.state = res[0].name;
                $scope.boothcode = res[0].ec_code;
                $scope.stateid = res[0].id;
                BlankFactory.getDistricts($scope.comaignid, $scope.stateid).then(function (res) {
                    $scope.district = res;
                    $scope.showassembly = 0;

                    $scope.boothcode = $scope.boothcode + res[0].ec_code;
                    $scope.districtid = res[0].hierarchy_level;

                })
            })
        })
    }
    function loaddistrict() {

    }
    //loadProofDetails();
    loadhierarchylevels();
    loaddistrict();
    // loadfunction();

    $scope.getAssembly = function (data) {
        BlankFactory.getAssembly(data.ditrict, $scope.districtid).then(function (res) {
            $scope.assemblies = res;
            $scope.showassembly = 1;
            $scope.showMandal = 0;
            $scope.showUpaMandal = 0;
            $scope.showBooth = 0;
            $scope.assemblieshid = res[0].hierarchy_level;
            getBWReports($scope.districtid);
        })
    }
    function getBWReports(id) {
        BlankFactory.getBWReports(id).then(function (res) {
            $scope.listview = res;
        })
    }
    $scope.getMandals = function (data) {
        BlankFactory.getMandals(data.assembly, $scope.assemblieshid).then(function (res) {
            $scope.mandals = res;
            $scope.showassembly = 1;
            $scope.showMandal = 0;
            $scope.showUpaMandal = 0;
            $scope.showBooth = 0;
            $scope.mandalid = res[0].hierarchy_level;
            getBWReports($scope.assemblieshid);
        })
    }
    $scope.getVillages = function (data) {
        BlankFactory.getVillages(data.mandal, $scope.mandalid).then(function (res) {
            $scope.villages = res;
            $scope.showUpaMandal = 1;
            $scope.showBoothnumber = 0;
            $scope.villageid = res[0].hierarchy_level;
            getBWReports($scope.mandalid);
        })
    }
    $scope.getBooths = function (data) {
        BlankFactory.getBooths(data.village, $scope.villageid).then(function (res) {
            $scope.booths = res;
            $scope.showBooth = 1;
            getBWReports($scope.villageid);

        })
    }
    $scope.getBoothNumber = function (data) {
        if (data.booths == "") {
            $scope.showBoothnumber = 1;
        }
        else {
            BlankFactory.getBoothNumber(data.booths).then(function (res) {
                // $scope.rg.boothNumber = res.booth_code;
                $scope.boothcode = data.booths;
                $scope.showBoothnumber = 1;
                getBWReports(data.booths);

            })
        }

    }
    $scope.modal = $ionicModal.fromTemplateUrl('modal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function (modal) {



        $scope.modal = modal;
    });
    $scope.openModal = function () {

        $scope.modal.show();
        // console.log(a)

    };
    $scope.closeModal = function () {
        $scope.modal.hide();
    };
    // Cleanup the modal when we're done with it!
    $scope.$on('$destroy', function () {
        $scope.modal.remove();
    });
    // Execute action on hide modal
    $scope.$on('modal.hidden', function () {
        // Execute action
    });

    $scope.$on('modal.removed', function () {
        // Execute action
    });
    $scope.modelpopup = function (data) {
        $scope.openModal();
        $scope.lists = data;

    }
    $scope.listview = function (data) {
        alert();
        $scope.openModal();

    }
}])
.controller('superadminCtrl', ['$scope', '$stateParams', '$state', '$ionicHistory', '$localStorage', '$rootScope', 'BlankFactory', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $state, $ionicHistory, $localStorage, $rootScope, BlankFactory) {
    $scope.GetReports = function () {
        $state.go("menu.reports");
    }
    $scope.addMember = function () {
        $state.go("menu.demo");
    }

    $scope.gotoWebsite = function () {
        BlankFactory.getwebsite($localStorage.userid, $localStorage.password).then(function (res) {
            if (res == "urlmsg") {
                window.location.href = 'http://apps.apbjpdata.org/';
                //window.open('http://188.42.96.92/', '_system');
                // var rf=  cordova.InAppBrowser.open('http://188.42.96.92/', "_system", "location=true");
                //ref.close();


            }

        })


    }
}])
 .controller('demoCtrl', ['$scope', '$stateParams', '$state', '$ionicHistory', '$localStorage', '$rootScope', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $state, $ionicHistory, $localStorage, $rootScope) {
    $scope.GetReports = function () {
        $state.go("menu.reports");
    }
    $scope.addMember = function () {
        $state.go("menu.demo");
    }
}])

.controller('changemobilenumberCtrl', ['$scope', '$stateParams', '$state', '$ionicHistory', '$localStorage', '$rootScope','BlankFactory','$ionicPopup','$ionicLoading', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $state, $ionicHistory, $localStorage, $rootScope, BlankFactory, $ionicPopup, $ionicLoading) {
    $scope.GetReports = function () {
        $state.go("menu.reports");
    }
    $scope.addMember = function () {
        $state.go("menu.demo");
    }
    $scope.show = function () {
        $ionicLoading.show({
            template: 'Loading...',
            duration: 3000
        }).then(function () {
            console.log("The loading indicator is now displayed");
        });
    };
    $scope.hide = function () {
        $ionicLoading.hide().then(function () {
            console.log("The loading indicator is now hidden");
        });
    };
    function loaddata() {
        BlankFactory.getUserMobile($localStorage.userid).then(function (res) {
            $scope.user = res.data;
        })
    }
    loaddata()
	$scope.callNumber = function(number){
		//alert(number)
		window.plugins.CallNumber.callNumber(onSuccess, onError, number, false);
	}
	function onSuccess(result){
  console.log("Success:"+result);
}

function onError(result) {
  console.log("Error:"+result);
}
    $scope.changeMobileNumber = function (data) {
        $scope.show();
        if (data.membershipID !== data.confirmmembershipID) {
            $scope.hide();
            var alertPopup = $ionicPopup.alert({
                title: 'Change Mobile Number',

                template: "Membership ID miss match,Please re-enter"
            });
        }
        else {
            if (data.mobileNumber != data.confirm_mobile_number) {
                $scope.hide();
                var alertPopup = $ionicPopup.alert({
                    title: 'Change Mobile Number',

                    template: "Mobile Number miss match,Please re-enter"
                });
            }
            else {
                BlankFactory.changeMobileNumber(data, $localStorage.userid).then(function (res) {

                    if (res.data.msg == "Your profile have been updated") {
                        $scope.hide();
                        var alertPopup = $ionicPopup.alert({
                            title: 'Change Mobile Number',

                            template: "Your Profile updated successfully" + "<br>" + "username:" + res.data.username + "<br>" + "Password: " + res.data.password
                        });
                        alertPopup.then(function (res) {
                            if (res) {
                                $state.go("menu.homeforlogin");
                            }
                        })
                    }

                    if (res.data.membership_id == "Membership Id already registered") {
                        $scope.hide();
                        var alertPopup = $ionicPopup.alert({
                            title: 'Change Mobile Number',

                            template: "Make sure to get New Membership ID using your New Mobile No, by dialing 18002661001. Try again"
                        });
                    }
                    if (res.data.mobile_number == "Mobile Number already registered") {
                        $scope.hide();
                        var alertPopup = $ionicPopup.alert({
                            title: 'Change Mobile Number',

                            template: res.data.mobile_number[0]
                        });
                    }
                    if(res.data.mobile_number=="Invalid mobile number")
                        {
                             $scope.hide();
                            var alertPopup = $ionicPopup.alert({
                                title: 'Change Mobile Number',

                                template: "Mobile Number Should Start With (6/7/8/9) only"
                            });


                        }
                })
            }
        }
        if (window.Connection) {
            //  alert("rathna" + window.Connection)
            if (navigator.connection.type == Connection.NONE) {
                $scope.hide();
                var alertPopup = $ionicPopup.alert({
                    title: 'Internet',

                    template: "No Internet connection"
                });

            }
            else {
               
            }

        }

        
       
    }
}])
.controller('eventsCtrl', ['$scope', '$stateParams', '$state', '$ionicHistory', '$localStorage', '$rootScope', 'BlankFactory','$ionicLoading', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $state, $ionicHistory, $localStorage, $rootScope, BlankFactory,$ionicLoading) {
    $scope.show = function () {
        $ionicLoading.show({
            template: 'Loading...',
            duration: 3000
        }).then(function () {
            console.log("The loading indicator is now displayed");
        });
    };
    $scope.hide = function () {
        $ionicLoading.hide().then(function () {
            console.log("The loading indicator is now hidden");
        });
    };
    
    $scope.flashNews = function () {
        $scope.show()
        BlankFactory.getflashNews().then(function (res) {
            $scope.flashnews = res;
            $scope.hide();

        })

    }

    $scope.flashNews();

    $scope.getAttendance= function(data){
        $localStorage.eventlist=data;
        console.log(data);
        $state.go("menu.eventslist");

    }
}])
.controller('eventslistCtrl', ['$scope', '$stateParams', '$state', '$ionicHistory', '$localStorage', '$rootScope', 'BlankFactory', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $state, $ionicHistory, $localStorage, $rootScope, BlankFactory) {
    $scope.flashNews = function () {
        BlankFactory.getflashNews().then(function (res) {
            $scope.flashnews = res;

        })

    }

    $scope.flashNews();
    $scope.getScanPage = function(){
        $state.go("menu.eventattendance")
    }
    $scope.getFeedbackPage = function(){
        $state.go("menu.eventfeedback")
    }
    $scope.getUploadPage = function(){
        $state.go("menu.eventupload")
    }
   
}])
.controller('eventfeedbackCtrl', ['$scope', '$stateParams', '$state', '$ionicHistory', '$localStorage', '$rootScope', 'BlankFactory', '$ionicPopup', '$ionicLoading', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $state, $ionicHistory, $localStorage, $rootScope, BlankFactory, $ionicPopup, $ionicLoading) {
    $scope.rg={
        eid:"",
        subject:"",
        description:""
    }
    $scope.show = function () {
        $ionicLoading.show({
            template: 'Loading...',

        }).then(function () {
            console.log("The loading indicator is now displayed");
        });
    };
    $scope.hide = function () {
        $ionicLoading.hide().then(function () {
            console.log("The loading indicator is now hidden");
        });
    };
    $scope.geteventlist=function(){
        BlankFactory.getflashNews().then(function (res) {
            $scope.flashnews = res;

        })
    }
    $scope.geteventlist();
    
    $scope.uid= $localStorage.userid ;
    $scope.rg.eid = $localStorage.eventlist.id.toString();
    $scope.eventname = $localStorage.eventlist.name;
    $scope.submit = function (data) {
        $scope.show();
        BlankFactory.eventFeedbackInsert($scope.uid, $localStorage.eventlist.id,data).then(function (res) {
            if (res.data.msg == "Feedback Saved Successfully") {
                $scope.hide();
                var alertPopup = $ionicPopup.alert({
                    title: 'Event Feedback',

                    template: "Feedback Saved Successfully"
                });

                $scope.rg.description="";
                $scope.rg.subject="";

            }
           


        })
    }

}])
.controller('eventuploadCtrl', ['$scope', '$stateParams', '$state', '$ionicHistory', '$localStorage', '$rootScope', 'BlankFactory', '$ionicPopup', '$ionicLoading', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $state, $ionicHistory, $localStorage, $rootScope, BlankFactory, $ionicPopup, $ionicLoading) {
    $scope.rg={
        eid:"",
        description:""
    }
    $scope.geteventlist=function(){
        BlankFactory.getflashNews().then(function (res) {
            $scope.flashnews = res;

        })
    }
    $scope.show = function () {
        $ionicLoading.show({
            template: 'Loading...',

        }).then(function () {
            console.log("The loading indicator is now displayed");
        });
    };
    $scope.hide = function () {
        $ionicLoading.hide().then(function () {
            console.log("The loading indicator is now hidden");
        });
    };
    $scope.files = []; 
    $scope.geteventlist();
    
    $scope.uid= $localStorage.userid ;
    $scope.eid = $localStorage.eventlist.id;
    $scope.eventname = $localStorage.eventlist.name;
    $scope.uploadFiles = function (files) {
        $scope.files = files;
        
    };
    
    $scope.submit = function () {
        $scope.show();
        $scope.file=document.getElementById('file').files;
        console.log($scope.file)
        if($scope.file.length !=0){
            BlankFactory.eventUploadPhotosInsert($scope.uid,$scope.eid,$scope.file).then(function(res){
                if (res.data.msg == "photos upload successfully") {
                    $scope.hide();
                    var alertPopup = $ionicPopup.alert({
                        title: 'Event Images',
    
                        template: "photos upload successfully"
                    });
                
                    alertPopup.then(function(res){
                        if(res){
                            $state.go('menu.eventslist')
                        }
                    })
    
                }
               
    
    
            })
        }
        else{
            $scope.hide();
            var alertPopup = $ionicPopup.alert({
                title: 'Event Images',

                template: "Please select photo"
            });
        }
        
        
    }
}])
.controller('eventattendanceCtrl', ['$scope', '$stateParams', '$state', '$ionicHistory', '$localStorage', '$rootScope', 'BlankFactory', '$cordovaBarcodeScanner', '$ionicPopup', '$ionicLoading', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $state, $ionicHistory, $localStorage, $rootScope, BlankFactory, $cordovaBarcodeScanner, $ionicPopup, $ionicLoading) {
   
    $scope.rg={
        name:"",
        membershipid:"",
        mobilenumber:"",
        address:""
        }
    $scope.show = function () {
        $ionicLoading.show({
            template: 'Loading...',

        }).then(function () {
            console.log("The loading indicator is now displayed");
        });
    };
    $scope.hide = function () {
        $ionicLoading.hide().then(function () {
            console.log("The loading indicator is now hidden");
        });
    };
    $scope.geteventlist=function(){
        BlankFactory.getflashNews().then(function (res) {
            $scope.flashnews = res;

        })
    }
    $scope.geteventlist();
     $scope.scandetails=0;
    $scope.eid=$localStorage.eventlist.id.toString();
    $scope.eventname = $localStorage.eventlist.name;
    $scope.scan = function () {
        $cordovaBarcodeScanner
      .scan()
      .then(function (result) {
          var data = JSON.parse(result.text)
          // var alertPopup = $ionicPopup.alert({
            // title: 'QR Scan',

            // template: "<strong>Name</strong> : " + data["Name"] + "<br>" + "<strong>Membership ID</strong> : " + data['membershi_id'] + "<br>" + "<strong>Mobile Number</strong> : " + data['mobile_number'] + "<br>" + "<strong>Address</strong> : " + data['Address'] + "<br>" + "<strong>Age</strong> : " + data['Age'] + "<br>" + "<strong>Caste</strong> : " + data["Caste"] + "<br>" + "<strong>Gender</strong> : " + data['Gender']
        // });
          
         
          $scope.rg.name=data["Name"];
          $scope.rg.membershipid=data["membershi_id"];

          $scope.rg.mobilenumber=data["mobile_number"];        
            $scope.rg.age=data["Age"];
            $scope.uid = data["uid"];
			
            
            $scope.scandetails=1;


          
          
      
          console.log(result)
      }, function (error) {
          // An error occurred
      });
    }

    $scope.submit=function(){
        $scope.show();
        BlankFactory.eventAttendanceInsert($scope.uid,$scope.eid).then(function(res){
            if (res.data.msg == "Attendance Exist") {
                $scope.hide();
                var alertPopup = $ionicPopup.alert({
                    title: 'Event Attendance',

                    template: "Attendance Exist"
                });
                alertPopup.then(function(res){
                    if(res){
                        $scope.scandetails=0;
                    }
                })

            }
            if (res.data.msg == "Attendance saved Succesfully") {
                $scope.hide();
                var alertPopup = $ionicPopup.alert({
                    title: 'Event Attendance',

                    template: "Attendance saved Succesfully"
                });
                alertPopup.then(function(res){
                    if(res){
                        $scope.scandetails=0;
                    }
                })

                
                            }


        })
    }

}])
.controller('feedbackCtrl', ['$scope', '$stateParams', '$state', '$ionicHistory', '$localStorage', '$rootScope', 'BlankFactory', "$ionicPopup", '$ionicLoading', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $state, $ionicHistory, $localStorage, $rootScope, BlankFactory, $ionicPopup, $ionicLoading) {
 $scope.rg={
    subject:"",
    description:"",
    catlist:""
 }
 $scope.show = function () {
     $ionicLoading.show({
         template: 'Loading...',

     }).then(function () {
         console.log("The loading indicator is now displayed");
     });
 };
 $scope.hide = function () {
     $ionicLoading.hide().then(function () {
         console.log("The loading indicator is now hidden");
     });
 };
$scope.orig = angular.copy($scope.rg);
    $scope.reset = function () {

        $scope.rg = angular.copy($scope.orig);

       
       
    }

    $scope.getfeedbackcategoryList = function () {
        BlankFactory.getfeedbackcategoryList().then(function (res) {
            $scope.catList = res;

        })

    }

    $scope.getfeedbackcategoryList();
    $scope.submit = function (data) {
        $scope.show();
    BlankFactory.addFeedback(data,$localStorage.userid).then(function(response){
        if (response.msg == "Feedback submitted succesfully") {
            $scope.hide();
            var alertPopup = $ionicPopup.alert({
                            title: 'Feedback',

                            template: "Feedback submitted succesfully"
                        });
             alertPopup.then(function (res) {
                    if (res) {
                               $scope.reset()
                         }
                        })
        }
    })
 }

}])
.controller('VoterlistCtrl', ['$scope', '$stateParams', '$state', '$ionicHistory', '$localStorage', '$rootScope', 'BlankFactory', "$ionicPopup", '$ionicLoading', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $state, $ionicHistory, $localStorage, $rootScope, BlankFactory, $ionicPopup, $ionicLoading) {
 
    $scope.show = function () {
        $ionicLoading.show({
            template: 'Loading...',
   
        }).then(function () {
            console.log("The loading indicator is now displayed");
        });
    };
    $scope.hide = function () {
        $ionicLoading.hide().then(function () {
            console.log("The loading indicator is now hidden");
        });
    };
    $scope.getVoterListByUserId = function () {
        $scope.show();
        BlankFactory.getVoterListByUserId($localStorage.userid,"userid").then(function (res) {
            $scope.voterlist = res.data;
            $scope.hide();

        })

    }
    $scope.getVoterListByUserId();


    
 

}])
.controller('activemembershipCtrl', ['$scope', '$stateParams', '$state', '$ionicHistory', '$localStorage', '$rootScope', 'BlankFactory', "$ionicPopup", '$ionicLoading', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $state, $ionicHistory, $localStorage, $rootScope, BlankFactory, $ionicPopup, $ionicLoading) {
 $scope.rg={
    f_s_name:"",
    caste:"",
    member_since:"",
    education:"",
    dob:"",
    transaction_number:"",
    doorno:"",
    street:"",
    pincode:""

 }
 $scope.pinFormat = /^[5][0-9]{5}$/;
 // $scope.emailFormat = /^(?!\.)[a-zA-Z0-9_,]*\.?[\.?a-zA-Z0-9_]+@[a-zA-Z]+\.(([a-zA-Z]{3})|([a-zA-Z]{2}\.[a-zA-Z]{2}))$/;

 $scope.membersincelist=[]
 $scope.list= function(){
    for(i=1980 ; i<=2018;i++){
        $scope.membersincelist.push(i)
    }
 }
 $scope.list()

 $scope.educationsList =[{"name":"SSC"},{"name":"INTER"},{'name':'DIPLOMA'},{"name":"GRADUATE"},{"name":"PG"},{"name":"PHD"},{'name':'No'}]
 $scope.show = function () {
     $ionicLoading.show({
         template: 'Loading...',

     }).then(function () {
         console.log("The loading indicator is now displayed");
     });
 };
 $scope.hide = function () {
     $ionicLoading.hide().then(function () {
         console.log("The loading indicator is now hidden");
     });
 };
$scope.orig = angular.copy($scope.rg);
    $scope.reset = function () {

        $scope.rg = angular.copy($scope.orig);

       
       
    }

    $scope.getfeedbackcategoryList = function () {
        BlankFactory.getfeedbackcategoryList().then(function (res) {
            $scope.catList = res;

        })

    }

    $scope.getfeedbackcategoryList();
    $scope.activeMember= function(){
        

        BlankFactory.getActiveMemberStatus($localStorage.userid).then(function (res) {
            if(res.status){
               
                if (res.booth_name !=""){
                    var alertPopup = $ionicPopup.alert({
                        title: 'Active Membership',
          
                        template:"Your Active Membership Status <strong>"+ res.msg+"</strong>"
                    });
                }
                else{
                    var alertPopup = $ionicPopup.alert({
                        title: 'Active Membership',
          
                        template:"Please Update Your Booth"
                    });
                    alertPopup.then(function(res){
                        if(res){
                            $state.go("menu.myaccount")
                        }
                    })
                }
                 

            }
            else{
                //$state.go("menu.activemebership");
            }

        }, function (msg) {
            //alert();
            //$state.go("menu.cart");
        })
       

    }
   // $scope.activeMember();

    $scope.imageUpload = function (event) {

        var files = event.target.files; //FileList object
        var file = files[0];
        var reader = new FileReader();
        $scope.selectImage = 1;
        reader.onload = $scope.imageIsLoaded;
        reader.readAsDataURL(file);
       
    }
    $scope.imageIsLoaded = function (e) {
        $scope.$apply(function () {
            $scope.stepsModel = (e.target.result);

            $scope.isImageUpload = 0;
        });
    }
    $scope.receiptimage="";
    $scope.selectReceiptImage=0;

    $scope.receiptImage = function(event){
        var files = event.target.files; //FileList object
        var file = files[0];
        var reader = new FileReader();
        $scope.selectReceiptImage = 1;
        reader.onload = $scope.reciptimageIsLoaded;
        reader.readAsDataURL(file);
    }
    $scope.reciptimageIsLoaded = function (e) {
        $scope.$apply(function () {
            $scope.receiptimage = (e.target.result);

            //$scope.isImageUpload = 0;
        });
    }
    function loadUserData(){
        BlankFactory.getUserDetails($localStorage.userid).then(function (response) {
            console.log(response)
            $scope.rg.f_s_name=response.f_s_name;
            $scope.rg.caste = response.usercategory_id.toString();
            $scope.rg.education = response.education.toString();
            $scope.rg.doorno = response.door_number;
            $scope.rg.pincode = response.pincode;
            $scope.rg.street = response.street;
            $scope.rg.member_since = response.member_since.toString();
            
            if(response.photo !=""){
                $scope.selectImage=1;
                $scope.stepsModel = response.photo;
            }
            
            $scope.rg.dob = new Date(response.dob);
            $scope.rg.member_since = response.member_since.toString();



        })
    }
    loadUserData();
    function loadCastDetails() {
        BlankFactory.getAllCast().then(function (res) {
            $scope.casts = res;
        })
    }
    loadCastDetails();
        var today = new Date();
    var minAge = 18;
    $scope.date = new Date(today.getFullYear() - minAge, today.getMonth(), today.getDate());
    // var maxAge = 99;
    // $scope.date = new Date(today.getFullYear() - maxAge, today.getMonth(), today.getDate());

    $scope.submit = function (data) {
        $scope.show();
        var file = document.getElementById("file").files[0];
        console.log(file)
        var receiptFile = document.getElementById("receiptimage").files[0];
        var dateofbirth=data.dob;
        dateofbirth=dateofbirth.getFullYear()+"-"+dateofbirth.getMonth()+"-"+dateofbirth.getDay();
        

    BlankFactory.activeMembership(data,file,dateofbirth, receiptFile,$localStorage.userid).then(function(response){
        if (response.data.msg) {
            $scope.hide();
            var alertPopup = $ionicPopup.alert({
                            title: 'Active Membership',

                            template:"Request raised successfully<br><small>Your request for Active Membership is registered successfully. We will get back to you shortly.</small>"
                        });
             alertPopup.then(function (res) {
                    if (res) {
                               $scope.reset()
                               $state.go('menu.homeforlogin')
                         }
                        })
        }
    })
 }

}])
.controller('menuCtrl', ['$scope', '$stateParams', '$ionicPopover', '$state', '$localStorage', '$rootScope', '$window', '$ionicHistory', '$templateCache', 'BlankFactory', '$ionicPopup', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $ionicPopover, $state, $localStorage, $rootScope, $window, $ionicHistory, $templateCache, BlankFactory, $ionicPopup) {
    $scope.popoverid = 0;
    // var popoverid = $localStorage.userid;
    //  console.log(popoverid);
    //$rootScope.loginresult = true;
    $scope.userid = $localStorage.userid;
    console.log($scope.userid);
    $rootScope.loginresult = 0;
    if ($scope.userid == undefined) {
        $rootScope.loginresult = 0;
        console.log($rootScope.loginresult);
    }
    else {
        $rootScope.loginresult = $localStorage.loginresults;
        console.log("rathna------------" + $rootScope.loginresult)
    }




    $scope.registration = function () {

        $ionicHistory.clearCache().then(function () { $state.go('menu.registration') });

    }


    $scope.loginpage = function () {
        $localStorage.loginresults = 0;
        $state.go("menu.login");
        $ionicHistory.nextViewOptions({
            disableBack: true
        });

    }
    $scope.logout = function () {
        $window.localStorage.clear();
        $ionicHistory.clearCache();
        $ionicHistory.clearHistory();
        $localStorage.$reset();
        $templateCache.removeAll();
        $scope.popoverid = 0;
        $localStorage.village_name="";


        $localStorage.loginresults = 0;
        console.log("reddy--" + $localStorage.loginresults);
        $state.go("menu.home");
        $rootScope.loginresult = $localStorage.loginresults;
        $ionicHistory.nextViewOptions({
            disableBack: true
        });
    }
    $scope.changePassword = function () {
        $state.go("menu.changepassword");

    }
    $scope.myAccount = function () {
        $state.go("menu.myaccount");

        $ionicHistory.nextViewOptions({
            disableBack: true
        });

    }
    $scope.getNews = function () {
        BlankFactory.getNews().then(function (res) {
            $rootScope.newsData = res;
            $state.go("menu.cart");

        })
    }
    $scope.changeMobileNumber = function () {
        $state.go("menu.changemobilenumber");
    }
}])
.controller('vistarakCtrl', ['$scope', '$stateParams', '$state', '$ionicHistory', 'BlankFactory','$localStorage', '$rootScope','$ionicLoading','$ionicPopup','$cordovaGeolocation','$ionicModal',
function ($scope, $stateParams, $state, $ionicHistory, BlankFactory,$localStorage, $rootScope,$ionicLoading,$ionicPopup,$cordovaGeolocation,$ionicModal) {

    $scope.rg = {
        "conducted_on": "",
        "duration": "",
        "type_of_report": "",
        "conducted_at": "",
        "longitude": "",
        "latitude": "",
        "male_attendence": "",
        "female_attendence": "",
        "oc_caste_attendence": "",
        "bc_caste_attendence": "",
        "christian_attendence": "",
        "muslim_attendence": "",
        "others_attendence": "",
        "new_memberships_attendence": "",
        "sc_attendence": "",
        "st_attendence": "",
       
        "total":"",
        "key_person":[],
        "schedule_time":"",
        "venue":"",
        "summary": "",
    }
    $scope.kp={
        "name":"",
        "mobile_no":"",
        "position":"",
       
    }
    var lat ,long="";
    var posOptions = {timeout: 10000, enableHighAccuracy: false};
    $cordovaGeolocation
    .getCurrentPosition(posOptions)
     
    .then(function (position) {
        lat  = position.coords.latitude
        long = position.coords.longitude
       console.log(lat + '   ' + long)
    }, function(err) {
       console.log(err)
    });
 
    var watchOptions = {timeout : 3000, enableHighAccuracy: false};
    var watch = $cordovaGeolocation.watchPosition(watchOptions);
     
    watch.then(
       null,
         
       function(err) {
          console.log(err)
       },
        function(position) {
           lat  = position.coords.latitude
           long = position.coords.longitude
          console.log(lat + '' + long)
       }
    );
 
    watch.clearWatch();

    function parseMillisecondsIntoReadableTime(milliseconds){
        //Get hours from milliseconds
        var hours = milliseconds / (1000*60*60);
        var absoluteHours = Math.floor(hours);
        var h = absoluteHours > 9 ? absoluteHours : '0' + absoluteHours;
      
        //Get remainder from hours and convert to minutes
        var minutes = (hours - absoluteHours) * 60;
        var absoluteMinutes = Math.floor(minutes);
        var m = absoluteMinutes > 9 ? absoluteMinutes : '0' +  absoluteMinutes;
      
        //Get remainder from minutes and convert to seconds
        var seconds = (minutes - absoluteMinutes) * 60;
        var absoluteSeconds = Math.floor(seconds);
        var s = absoluteSeconds > 9 ? absoluteSeconds : '0' + absoluteSeconds;
      
      
        return h + ':' + m + ':' + s;
      }
      
      
      
$scope.reporttypes=[{id:1,name:"PB"},{id:2,name:"Village"},{id:3,name:"SK"},{id:4,name:"Mandal"},{id:5,name:"Assembly"},{id:6,name:"Parliament"}, {id:7,name:"District"},{id:8,name:"State"},]
$scope.duration_type=[{id:1,name:"1"},{id:2,name:"2"},{id:3,name:"3"},{id:4,name:"4"},{id:5,name:"5"},{id:6,name:"6"}, {id:7,name:"7"},{id:8,name:"8"},{id:7,name:"9"},{id:8,name:"10"}]
$scope.submit = function (req) {
    console.log(req)
    //console.log($scope.rg.schedule_time.getTime())
    $scope.rg.schedule_time = parseMillisecondsIntoReadableTime($scope.rg.schedule_time)
    var date=new Date($scope.rg.conducted_on);
    var month;
    if (date.getMonth() == 12){
        month =1
    }
    else{
        month=date.getMonth()+1
    }
    date=date.getFullYear()+"-"+month+"-"+date.getDay();
    req['conducted_on'] = date
    req['longitude'] =lat;
    req['latitude']=long;
    
    BlankFactory.vistarakReport(req,$localStorage.userid).then(function(res){
      console.log(res)
        if (res.success) {
            var alertPopup = $ionicPopup.alert({
                title: 'Vistarak',
                cssClass: 'button-royal',
                template: res.msg

            });
            alertPopup.then(function (res) {
                if (res) {
                    $state.go("menu.homeforlogin");

                    $ionicHistory.nextViewOptions({
                        disableBack: true
                    });
                }
            })
        }
        else{
            var alertPopup = $ionicPopup.alert({
                title: 'Vistarak',
                cssClass: 'button-royal',
                template: res.msg

            });
            // alertPopup.then(function (res) {
            //     if (res) {
            //         $state.go("menu.homeforlogin");

            //         $ionicHistory.nextViewOptions({
            //             disableBack: true
            //         });
            //     }
            // })
        }
        
       

    })

}
$scope.keypersonlist=[]
$scope.addKeyPerson = function(data){

    $scope.rg.key_person.push($scope.kp)
    $scope.keypersonlist.push({"name":data.name,"mobile_no":data.mobile_no,"position":data.position})
    // document.getElementById('KeypersonForm').reset()
    $scope.reset();
    //$scope.reset()
   
    
}
$scope.orig = angular.copy($scope.kp);
$scope.reset = function () {

    $scope.kp = angular.copy($scope.orig);
   
    //$scope.KeypersonForm.$dirty = false;
   // $scope.KeypersonForm.$setPristine(  );
    //form.$setPristine();
    //document.getElementById("KeypersonForm").reset();
    //  $scope.MemberRegistration.$setPristine(true);
}
$ionicModal.fromTemplateUrl('my-modal.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal;
  });
  $scope.openModal = function() {
    $scope.modal.show();
  };
  $scope.closeModal = function() {
    $scope.modal.hide();
  };
  // Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.modal.remove();
  });
  // Execute action on hide modal
  $scope.$on('modal.hidden', function() {
    // Execute action
  });
  // Execute action on remove modal
  $scope.$on('modal.removed', function() {
    // Execute action
  });

}])